<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-01-22
 * Time: ���� 10:47
 */
$sub_menu = "200300";
//if (!defined('_EYOOM_IS_ADMIN_')) exit;

include_once('./_common.php');

/*auth_check($auth[$sub_menu], 'w');

check_demo();

check_admin_token();*/

require_once(G5_PATH.'/mail_service/std_mail_modules.php');
require_once(G5_PATH.'/mail_service/mailgun/Mailgun_send.php');
require_once(G5_PATH.'/mail_service/sendgrid/sendgrid-php/sendgrid-php.php');

$str_mail_modules= new stdmailmodules\std_mail_modules();
$dateinfo = trim($_REQUEST['date_info']);
$domain_info = trim($_REQUEST['domain_info']);
$api_key_info = trim($_REQUEST['api_key_info']);

//modify for mail service by lim
$sql = " select cf_1,cf_2,cf_3,cf_4,cf_5,cf_6,cf_7,cf_8,cf_9 from {$g5['config_table']} ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++) {
    $mail_service_info[$i] = $row;
}

if($dateinfo != null){
    $mailgun_time = $str_mail_modules->convertTimeForMailgun($dateinfo);
    $sendgrid_time = $str_mail_modules->convertTimeForSendgrid($dateinfo);
    if($mailgun_time == "error" || $sendgrid_time == "error" )
    {
        $err_msg='Sending reservation time is over then 3day';
        header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
        exit;
    }
}

//check selected mail service
if($mail_service_info[0]['cf_2'] == 1) {
    $selected_mail_service = 'mailgun';
}elseif($mail_service_info[0]['cf_6'] == 1){
    $selected_mail_service = 'sendgrid';
}else{
    include_once(G5_LIB_PATH.'/mailer.lib.php');
    $selected_mail_service = 'local_server';
}


//system parameter setting
$countgap = 10; // ��Ǿ� ������ ����
$sleepsec = 200;  // õ���� ���ʰ� ���� ����

$mailgun_sending_limit = $str_mail_modules->getMailgunSendingLimit();

if($api_key_info!=null){
    $mailgun_api_key = $api_key_info;
    $sendgrid_api_key = $api_key_info;
}else{
    $mailgun_api_key = $mail_service_info[0]['cf_4'];
    $sendgrid_api_key = $mail_service_info[0]['cf_8'];
}


$send_count= $mail_service_info[0]['cf_3'];
$over_send_set_state= $mail_service_info[0]['cf_5'];
$use_con_set_state= $mail_service_info[0]['cf_9'];
$last_sending_success_user_id='';
$sendgrid_contn_send_set=0;

if($domain_info!=null){
    $domain = $domain_info;
}else{
    $domain = "hellomilja.com";
}

//check mail service using flag state
$result = $str_mail_modules->CheckServiceOptionsState($send_count, $over_send_set_state, $use_con_set_state, $selected_mail_service);

if($result==1){
    $err_msg='maximum_milgun_sending_count';
    header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
    exit;
}
if($result==2){
    $err_msg='check_sendgrid_continue_sending_flag';
    header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
    exit;
}
if($result==3) {
    $sendgrid_contn_send_set = 1;
}

$ma_id = trim($_POST['ma_id']);

// ���ϳ��� ��������
$sql = "select ma_subject, ma_content from {$g5['mail_table']} where ma_id = '$ma_id' ";
$ma = sql_fetch($sql);

$subject = $ma['ma_subject'];

$select_member_list = trim($_REQUEST['msg']);
$member_list = explode(',', $select_member_list);

$cnt = 0;
for ($i=0; $i<count($member_list); $i++){
    list($name, $to_email) = explode(":", trim($member_list[$i]));

    $sw = preg_match("/[0-9a-zA-Z_]+(\.[0-9a-zA-Z_]+)*@[0-9a-zA-Z_]+(\.[0-9a-zA-Z_]+)*/", $to_email);
    // �ùٸ� ���� �ּҸ�
    if ($sw == true) {

        $cnt++;

        $content = $ma['ma_content'];

        // switch mail service
        if ($selected_mail_service == 'mailgun' && $sendgrid_contn_send_set == 0) {
            $send_count++;

            $from_name = $config['cf_admin_email_name'];
            $from = $config['cf_admin_email'];
            $to_name = $name;
            $to = $to_email;
            if ($dateinfo != null)
                $delivery_time = $mailgun_time;
            else
                $delivery_time = '';

            $mailgun = new MailgunMailer();

         if (strpos($content, "div") == false) {
                //$content = $content . "<hr size=0><p><span style='font-size:9pt; font-familye:����'>�� �� �̻� ���� ������ ��ġ �����ø� [<a href='".G5_BBS_URL."/email_stop.php?mb_id={$mb_id}&amp;mb_md5={$mb_md5}' target='_blank'>���Űź�</a>] �� �ֽʽÿ�.</span></p>";
                $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,0,$mb_id,$mb_md5);
                $text = $content;
                if ($delivery_time == '')
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 0, $text, '', $cc = '', $bcc = '', '', $delivery_time = '');
                else
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 0, $text, '', $cc = '', $bcc = '', '', $delivery_time);
            } else {
                $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,1,$mb_id,$mb_md5);
                $html = $content;
                if ($delivery_time == '')
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 1, $html, '', $cc = '', $bcc = '', '', $delivery_time = '');
                else
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 1, $html, '', $cc = '', $bcc = '', '', $delivery_time);
            }

            if ($response == "Queued. Thank you.") {
                $sql = " update {$g5['config_table']} set cf_10 = '" . $to_email . "' where 1";
                sql_query($sql);
            } else {
                $err_msg = $member['mb_nick'] . '(' . $member['mb_email'] . ')_mailgun_sending_failed'.$response;
                header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
                exit;
            }

            if ($send_count == $mailgun_sending_limit) {
                $result = $str_mail_modules->CheckServiceOptionsState($send_count, $over_send_set_state, $use_con_set_state, $selected_mail_service);

                if ($result == 1) {
                    $err_msg = 'maximum_milgun_sending_count';
                    header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
                    exit;
                }

                if ($result == 2) {
                    $err_msg = 'check_sendgrid_continue_sending_flag';
                    header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
                    exit;
                }
                if ($result == 3) {
                    $sendgrid_contn_send_set = 1;
                }
            }
        } elseif ($selected_mail_service == 'sendgrid' || $sendgrid_contn_send_set == 1) {


            $api_key = trim($sendgrid_api_key);
            $from_name = $config['cf_admin_email_name'];
            $from = $config['cf_admin_email'];
            $from = new SendGrid\Email($from_name, $from);
            $to_name = $name;
            $to = $to_email;
            $to = new SendGrid\Email($to_name, $to);
            $subject = $subject;
            if ($dateinfo != null)
                $delivery_time = $sendgrid_time;
            else
                $delivery_time = '';

            if(strpos($content, "div") == false) {
                $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,0,$mb_id,$mb_md5);
                $content = new SendGrid\Content("text/plain", $content);
            }else{
                $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,1,$mb_id,$mb_md5);
                $content = new SendGrid\Content("text/html", $content);
            }

            $mail = new SendGrid\Mail($from, $subject, $to, $content);
            if ($delivery_time != '')
                $mail->setSendAt($delivery_time);

            $sg = new \SendGrid($api_key);
            $response = $sg->client->mail()->send()->post($mail);

            if ($response->statusCode() == "202") {
                $sql = " update {$g5['config_table']} set cf_10 = '" . $to_email . "' where 1";
                sql_query($sql);
            } else {
                $err_msg = $member['mb_nick'] . '(' . $member['mb_email'] . ')_sendgrid_sending_failed'.$response->body();
                header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
                exit;
            }

        } else {
            //$content = $content . "<hr size=0><p><span style='font-size:9pt; font-familye:����'>�� �� �̻� ���� ������ ��ġ �����ø� [<a href='" . G5_BBS_URL . "/email_stop.php?mb_id={$mb_id}&amp;mb_md5={$mb_md5}' target='_blank'>���Űź�</a>] �� �ֽʽÿ�.</span></p>";
            $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,1,$mb_id,$mb_md5);
            mailer($config['cf_admin_email_name'], $config['cf_admin_email'], $to_email, $subject, $content, 1);
            //update succeed sending user id
            $sql = " update {$g5['config_table']} set cf_10 = '" . $to_email . "' where 1";
            sql_query($sql);
        }
    }
    usleep($sleepsec);
}

$err_msg = 'Successful transfer complete';
header('location:'.G5_ADMIN_URL.'/mail_list.php?error_msg='.$err_msg);
exit;