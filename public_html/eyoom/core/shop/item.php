<?php
	if (!defined('_SHOP_')) exit;
	
	if (G5_IS_MOBILE && $eyoom['use_shop_mobile'] == 'y') {
		include_once(EYOOM_MSHOP_PATH.'/item.php');
		return;
	}
	
	$it_id = trim($_GET['it_id']);
	
	include_once(G5_LIB_PATH.'/iteminfo.lib.php');
	
	// 분류사용, 상품사용하는 상품의 정보를 얻음
	$sql = " select a.*, b.ca_name, b.ca_use from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b where a.it_id = '$it_id' and a.ca_id = b.ca_id ";
	$it = sql_fetch($sql);
	if (!$it['it_id'])
		alert('자료가 없습니다.');
	if (!($it['ca_use'] && $it['it_use'])) {
		if (!$is_admin)
			alert('현재 판매가능한 상품이 아닙니다.');
	}
	
	// 분류 테이블에서 분류 상단, 하단 코드를 얻음
	$sql = " select ca_skin_dir, ca_include_head, ca_include_tail, ca_cert_use, ca_adult_use from {$g5['g5_shop_category_table']} where ca_id = '{$it['ca_id']}' ";
	$ca = sql_fetch($sql);
	
	// 본인인증, 성인인증체크
	if(!$is_admin) {
		$msg = shop_member_cert_check($it_id, 'item');
		if($msg)
			alert($msg, G5_SHOP_URL);
	}
	
	// 오늘 본 상품 저장 시작
	// tv 는 today view 약자
	$saved = false;
	$tv_idx = (int)get_session("ss_tv_idx");
	if ($tv_idx > 0) {
		for ($i=1; $i<=$tv_idx; $i++) {
			if (get_session("ss_tv[$i]") == $it_id) {
				$saved = true;
				break;
			}
		}
	}
	
	if (!$saved) {
		$tv_idx++;
		set_session("ss_tv_idx", $tv_idx);
		set_session("ss_tv[$tv_idx]", $it_id);
	}
	// 오늘 본 상품 저장 끝
	
	// 조회수 증가
	if (get_cookie('ck_it_id') != $it_id) {
		sql_query(" update {$g5['g5_shop_item_table']} set it_hit = it_hit + 1 where it_id = '$it_id' "); // 1증가
		set_cookie("ck_it_id", $it_id, time() + 3600); // 1시간동안 저장
	}
	
	define('G5_SHOP_CSS_URL', str_replace(G5_PATH, G5_URL, $skin_dir));
	
	$g5['title'] = $it['it_name'].' &gt; '.$it['ca_name'];
	
	// 그누 헤더정보 출력
	@include_once(G5_PATH.'/head.sub.php');
	
	if ($ca['ca_include_head']) {
		@include_once($ca['ca_include_head']);
	} else {
		// 이윰 테일 디자인 출력
		@include_once(EYOOM_SHOP_PATH.'/shop.head.php');
	}
	
	/**** item : Start ****/
	// 상품기본정보 및 주문폼
	@include_once(EYOOM_SHOP_PATH.'/item_form.skin.php');
	
	// 상품 상세정보
	@include_once(EYOOM_SHOP_PATH.'/item_info.skin.php');
	
	$tpl->define(array(
		'item_form_pc'	=> 'skin_pc/shop/' . $eyoom['shop_skin'] . '/item_form.skin.html',
		'item_form_mo'	=> 'skin_mo/shop/' . $eyoom['shop_skin'] . '/item_form.skin.html',
		'item_form_bs'	=> 'skin_bs/shop/' . $eyoom['shop_skin'] . '/item_form.skin.html',
		'item_info_pc'	=> 'skin_pc/shop/' . $eyoom['shop_skin'] . '/item_info.skin.html',
		'item_info_mo'	=> 'skin_mo/shop/' . $eyoom['shop_skin'] . '/item_info.skin.html',
		'item_info_bs'	=> 'skin_bs/shop/' . $eyoom['shop_skin'] . '/item_info.skin.html',
		'item_use_pc'	=> 'skin_pc/shop/' . $eyoom['shop_skin'] . '/item_use.skin.html',
		'item_use_mo'	=> 'skin_mo/shop/' . $eyoom['shop_skin'] . '/item_use.skin.html',
		'item_use_bs'	=> 'skin_bs/shop/' . $eyoom['shop_skin'] . '/item_use.skin.html',
		'item_qa_pc'	=> 'skin_pc/shop/' . $eyoom['shop_skin'] . '/item_qa.skin.html',
		'item_qa_mo'	=> 'skin_mo/shop/' . $eyoom['shop_skin'] . '/item_qa.skin.html',
		'item_qa_bs'	=> 'skin_bs/shop/' . $eyoom['shop_skin'] . '/item_qa.skin.html',
		'item_rel_pc'	=> 'skin_pc/shop/' . $eyoom['shop_skin'] . '/item_rel.skin.html',
		'item_rel_mo'	=> 'skin_mo/shop/' . $eyoom['shop_skin'] . '/item_rel.skin.html',
		'item_rel_bs'	=> 'skin_bs/shop/' . $eyoom['shop_skin'] . '/item_rel.skin.html',
	));
	/**** item : End ****/
	
	// Template define
	$tpl->define_template('shop',$eyoom['shop_skin'],'item.skin.html');

	// 사용자 프로그램
	@include_once(EYOOM_USER_PATH.'/shop/item.php');
	
	// Template assign
	@include EYOOM_INC_PATH.'/tpl.assign.php';
	$tpl->print_($tpl_name);
	
	if ($ca['ca_include_tail']) {
		@include_once($ca['ca_include_tail']);
	} else {
		// 이윰 테일 디자인 출력
		@include_once(EYOOM_SHOP_PATH.'/shop.tail.php');
	}
?>