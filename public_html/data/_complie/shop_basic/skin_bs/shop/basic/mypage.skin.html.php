<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/mypage.skin.html 000004838 */ 
$TPL_wishlist_1=empty($TPL_VAR["wishlist"])||!is_array($TPL_VAR["wishlist"])?0:count($TPL_VAR["wishlist"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="shop-mypage">
<section class="mypage-info margin-bottom-30">
<div class="headline"><h5><strong>쇼핑몰 내 정보</strong></h5></div>
<div class="text-right margin-bottom-20">
<?php if($GLOBALS["is_admin"]=='super'){?><a href="<?php echo G5_ADMIN_URL?>/" class="btn-e btn-e-purple margin-right-5">관리자</a><?php }?>
<a href="<?php echo G5_BBS_URL?>/memo.php" target="_blank" class="win_memo btn-e btn-e-dark">쪽지함</a>
<a href="<?php echo G5_BBS_URL?>/member_confirm.php?url=register_form.php" class="btn-e btn-e-red">회원정보수정</a>
<a href="<?php echo G5_BBS_URL?>/member_confirm.php?url=member_leave.php" onclick="return member_leave();" class="btn-e btn-e-default">회원탈퇴</a>
</div>
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb">
<div class="table-responsive">
<table id="sod_list" class="table table-bordered">
<thead>
<tr>
<th>항목</th>
<th>내용</th>
</tr>
</thead>
<tbody>
<tr>
<th>보유포인트</th>
<td><?php echo number_format($TPL_VAR["member"]["mb_point"])?>점 <a href="<?php echo G5_BBS_URL?>/point.php" target="_blank" class="win_point btn-e btn-e-default btn-e-xs margin-left-5"><span class="color-white">포인트내역보기</span></a></td>
</tr>
<tr>
<th>보유쿠폰</th>
<td><?php echo number_format($GLOBALS["cp_count"])?>개 <a href="<?php echo G5_SHOP_URL; ?>/coupon.php" target="_blank" class="win_coupon btn-e btn-e-default btn-e-xs margin-left-5"><span class="color-white">쿠폰내역보기</span></a></td>
</tr>
<tr>
<th>연락처</th>
<td><?php if($TPL_VAR["member"]["mb_tel"]){?><?php echo $TPL_VAR["member"]["mb_tel"]?><?php }else{?>미등록<?php }?></td>
</tr>
<tr>
<th>E-Mail</th>
<td><?php if($TPL_VAR["member"]["mb_email"]){?><?php echo $TPL_VAR["member"]["mb_email"]?><?php }else{?>미등록<?php }?></td>
</tr>
<tr>
<th>최종접속일시</th>
<td><?php echo $TPL_VAR["member"]["mb_today_login"]?></td>
</tr>
<tr>
<th>회원가입일시</th>
<td><?php echo $TPL_VAR["member"]["mb_datetime"]?></td>
</tr>
<tr>
<th id="smb_my_ovaddt">주소</th>
<td id="smb_my_ovaddd"><?php echo sprintf("(%s%s)",$TPL_VAR["member"]["mb_zip1"],$TPL_VAR["member"]["mb_zip2"])?> <?php echo print_address($TPL_VAR["member"]["mb_addr1"],$TPL_VAR["member"]["mb_addr2"],$TPL_VAR["member"]["mb_addr3"],$TPL_VAR["ember"]["mb_addr_jibeon"])?></td>
</tr>
</tbody>
</table>
</div>
</div>
</section>
<section class="mypage-order margin-bottom-30">
<div class="headline"><h5><strong>최근 주문내역</strong></h5></div>
<?php $this->print_("orderinquiry_sub_bs",$TPL_SCP,1);?>
<div class="text-right">
<a href="./orderinquiry.php" class="btn-e btn-e-dark btn-e-xs">주문내역 더보기</a>
</div>
</section>
<section class="mypage-wish margin-bottom-30">
<div class="headline"><h5><strong>최근 위시리스트</strong></h5></div>
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb">
<div class="table-responsive">
<table class="table table-bordered">
<thead>
<tr>
<th>이미지</th>
<th>상품명</th>
<th>보관일시</th>
</tr>
</thead>
<tbody>
<?php if($TPL_wishlist_1){foreach($TPL_VAR["wishlist"] as $TPL_V1){?>
<tr>
<td class="smb-my-img"><a href="./item.php?it_id=<?php echo $TPL_V1["it_id"]?>"><?php echo $TPL_V1["image"]?></a></td>
<td><a href="./item.php?it_id=<?php echo $TPL_V1["it_id"]?>"><?php echo stripslashes($TPL_V1["it_name"])?></a></td>
<td><?php echo $TPL_V1["wi_time"]?></td>
</tr>
<?php }}else{?>
<tr><td colspan="3" class="text-center">보관 내역이 없습니다.</td></tr>
<?php }?>
</tbody>
</table>
</div>
</div>
<div class="text-right">
<a href="./wishlist.php" class="btn-e btn-e-dark btn-e-xs">위시리스트 더보기</a>
</div>
</section>
</div>
<style>
.shop-mypage .mypage-info thead th {text-align:left !important}
.shop-mypage .mypage-info #sod_list thead th {padding:8px}
.shop-mypage .mypage-wish .table-list-eb .smb-my-img {width:80px}
.shop-mypage .mypage-wish .table-list-eb td img {display:block;width:100%;max-width:100%;height:auto}
</style>
<script>
$(function() {
$(".win_coupon").click(function() {
var new_win = window.open($(this).attr("href"), "win_coupon", "left=100,top=100,width=700, height=600, scrollbars=1");
new_win.focus();
return false;
});
});
function member_leave()
{
return confirm('정말 회원에서 탈퇴 하시겠습니까?')
}
</script>