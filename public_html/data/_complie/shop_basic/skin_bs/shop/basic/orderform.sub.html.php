<?php /* Template_ 2.2.8 2019/10/27 16:21:47 /home/hellomilja/www/eyoom/theme/shop_basic/skin_bs/shop/basic/orderform.sub.html 000048213 */ 
$TPL_sod_list_1=empty($TPL_VAR["sod_list"])||!is_array($TPL_VAR["sod_list"])?0:count($TPL_VAR["sod_list"]);
$TPL__latest_addr_1=empty($GLOBALS["latest_addr"])||!is_array($GLOBALS["latest_addr"])?0:count($GLOBALS["latest_addr"]);
$TPL__bank_account_1=empty($GLOBALS["bank_account"])||!is_array($GLOBALS["bank_account"])?0:count($GLOBALS["bank_account"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
.shop-product-order-form .color-red-t {color:#ae0000}
.shop-product-order-form .margin-hrd-5 {height:1px;border-top:1px dotted #536999;margin:5px 0;clear:both}
.shop-product-order-form .table-list-eb .sod-img{max-width:50px}
.shop-product-order-form .table-list-eb td img{display:block;width:100%;max-width:100%;height:auto}
.shop-product-order-form .sod-bsk-tot {background:#334773;border:1px solid #1F263B;color:#fff;padding:10px;margin-bottom:30px}
.shop-product-order-form .sod-bsk-tot p {padding:0 10px;margin-bottom:5px;color:#c5cced}
.shop-product-order-form .sod-bsk-tot-in {border:1px solid #1F263B;background:#e1e5fb;padding:8px 10px}
.shop-product-order-form input.btn_submit {background:#FF2900}
.sod-frm-orderer {position:relative}
.sod-frm-taker {position:relative}
.sod-frm-pay .table-list-eb td span {display:inline}
.sod-frm-paysel {position:relative;border:1px solid #b5b5b5;padding:10px;background:#f8f8f8;margin-bottom:20px}
.sod-frm-paysel fieldset {padding:0;background:none}
#sod_list #cp_frm {z-index:10000;position:absolute;top:0;left:0;padding:15px;width:100%;max-width:500px;height:auto !important;height:500px;max-height:500px;border:1px solid #aaa;background:#f4f4f4;overflow-y:scroll;overflow-x:none}
.shop-product-order-form .table-list-eb td.sod-img img {width:50px;margin:0 auto}
.shop-product-order-form .btn_confirm input[type="button"], .shop-product-order-form .btn_confirm a {white-space:nowrap;border:0;color:#fff;font-size:14px;cursor:pointer;font-weight:300;padding:7px 15px;position:relative;background:#FF2A00;display:inline-block;text-decoration:none}
#display_pay_button .btn_submit {padding:7px 15px}
.shop-product-order-form .btn_confirm input[type="button"]:hover, .shop-product-order-form .btn_confirm a:hover {color:#fff;background:#c7371a;text-decoration:none;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out}
.shop-product-order-form .btn_confirm a {background:#95959A}
.shop-product-order-form .btn_confirm a:hover {background:#75757A}
.shop-product-order-form label.radio {margin-bottom:0}
.shop-product-order-form .table-bordered>thead>tr>th, .shop-product-order-form .table-bordered>tbody>tr>th, .shop-product-order-form .table-bordered>tfoot>tr>th, .shop-product-order-form .table-bordered>thead>tr>td, .shop-product-order-form .table-bordered>tbody>tr>td, .shop-product-order-form .table-bordered>tfoot>tr>td {border:1px solid #bacdf8}
.shop-product-order-form .table-list-eb {color:#000}
.shop-product-order-form .table-list-eb .table tbody > tr > td {text-align:center}
.shop-product-order-form .table-list-eb .table tbody > tr > th {background:#e7efff;padding-left:5px;padding-right:5px}
.shop-product-order-form .table-list-eb thead {border-top:1px solid #bacdf8;background:#e7efff}
.shop-product-order-form .table-list-eb .table tbody > tr > td {border-top:1px solid #bacdf8}
/* Datepicker CSS 수정 */
.ui-datepicker {width:240px}
.ui-datepicker td span, .ui-datepicker td a {padding:inherit;text-align:inherit;line-height:25px}
.ui-widget-header {border:0;border-bottom:1px solid #c5c5c5 !important;background:#e5e5e5}
.ui-widget-content .ui-state-default {border:inherit;background:none}
.ui-datepicker .ui-datepicker-buttonpane button {margin:10px 0 0;padding:5px 15px;border:0;background:#171C29;color:#fff}
.ui-datepicker .ui-datepicker-buttonpane button:hover {background:#1F263B !important}
.ui-datepicker .ui-datepicker-prev:hover, .ui-datepicker .ui-datepicker-next:hover {border:0}
/* 영카트 모바일 기본 CSS 수정 */
#sod_frm #cp_frm, #od_coupon_frm, #sc_coupon_frm {z-index:1;position:relative;top:0;left:0;width:100%;border:0;background:#fff}
#sod_list thead th {padding:8px}
</style>
<?php echo $GLOBALS["orderform1"]?>
<?php if($GLOBALS["is_kakaopay_use"]){?>
<?php echo $GLOBALS["kakaopay_orderform1"]?>
<?php }?>
<form name="forderform" id="forderform" method="post" action="<?php echo $GLOBALS["order_action_url"]?>" autocomplete="off" class="eyoom-form">
<div class="shop-product-order-form">
<div class="note margin-bottom-10"><strong><i class="fa fa-exclamation-circle"></i> Note:</strong> 주문하실 상품을 확인하세요.</div>
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb margin-bottom-20">
<div class="table-responsive">
<table id="sod_list" class="table table-bordered">
<thead>
<tr>
<th>상품이미지</th>
<th>상품명</th>
<th>총수량</th>
<th>판매가</th>
<th>쿠폰</th>
<th>소계</th>
<th>포인트</th>
<th>배송비</th>
</tr>
</thead>
<tbody>
<?php if($TPL_sod_list_1){foreach($TPL_VAR["sod_list"] as $TPL_K1=>$TPL_V1){?>
<tr>
<td class="sod-img"><?php echo $TPL_V1["image"]?></td>
<td>
<input type="hidden" name="it_id[<?php echo $TPL_K1?>]"    value="<?php echo $TPL_V1["it_id"]?>">
<input type="hidden" name="it_name[<?php echo $TPL_K1?>]"  value="<?php echo get_text($TPL_V1["it_name"])?>">
<input type="hidden" name="it_price[<?php echo $TPL_K1?>]" value="<?php echo $TPL_V1["sell_price"]?>">
<input type="hidden" name="cp_id[<?php echo $TPL_K1?>]" value="">
<input type="hidden" name="cp_price[<?php echo $TPL_K1?>]" value="0">
<?php if($TPL_VAR["default"]["de_tax_flag_use"]){?>
<input type="hidden" name="it_notax[<?php echo $TPL_K1?>]" value="<?php echo $TPL_V1["it_notax"]?>">
<?php }?>
<?php echo $TPL_V1["it_name"]?>
</td>
<td><?php echo $TPL_V1["sum_qty"]?></td>
<td><?php echo number_format($TPL_V1["ct_price"])?></td>
<td><?php if($TPL_V1["cp_count"]){?><button type="button" class="cp_btn btn-e btn-e-dark btn-e-xs">적용</button><?php }?></td>
<td><span class="total_price"><?php echo number_format($TPL_V1["sell_price"])?></span></td>
<td><?php echo number_format($TPL_V1["point"])?></td>
<td><?php echo $TPL_V1["ct_send_cost"]?></td>
</tr>
<?php }}?>
</tbody>
</table>
</div>
</div>
<div class="sod-bsk-tot">
<p class="pull-left">주문</p>
<p class="pull-right"><strong class="color-white"><?php echo number_format($GLOBALS["tot_sell_price"])?> 원</strong></p>
<div class="margin-hrd-5"></div>
<?php if($GLOBALS["it_cp_count"]> 0){?>
<p class="pull-left">쿠폰할인</p>
<p class="pull-right"><strong class="color-white" id="ct_tot_coupon">0 원</strong></p>
<div class="margin-hrd-5"></div>
<?php }?>
<p class="pull-left">배송비</p>
<p class="pull-right"><strong class="color-white"><?php echo number_format($GLOBALS["send_cost"])?> 원</strong></p>
<div class="clearfix"></div>
<div class="sod-bsk-tot-in">
<span class="pull-left color-black"><strong>총계</strong></span>
<span class="pull-right color-red-t"><strong id="ct_tot_price"><?php echo number_format($GLOBALS["tot_price"])?> 원</strong></span>
<div class="clearfix"></div>
<span class="pull-left color-black"><strong>포인트</strong></span>
<span class="pull-right color-black"><strong><?php echo number_format($GLOBALS["tot_point"])?> 점</strong></span>
<div class="clearfix"></div>
</div>
</div>
<input type="hidden" name="od_price" value="<?php echo $GLOBALS["tot_sell_price"]?>">
<input type="hidden" name="org_od_price" value="<?php echo $GLOBALS["tot_sell_price"]?>">
<input type="hidden" name="od_send_cost" value="<?php echo $GLOBALS["send_cost"]?>">
<input type="hidden" name="od_send_cost2" value="0">
<input type="hidden" name="item_coupon" value="0">
<input type="hidden" name="od_coupon" value="0">
<input type="hidden" name="od_send_coupon" value="0">
<?php echo $GLOBALS["orderform2"]?>
<?php if($GLOBALS["is_kakaopay_use"]){?>
<?php echo $GLOBALS["kakaopay_orderform2"]?>
<?php }?>
<div class="headline"><h5><strong>주문하시는 분</strong></h5></div>
<section class="sod-frm-orderer">
<div class="row">
<section class="col col-6">
<label for="od_name" class="label">이름<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-user"></i>
<input type="text" name="od_name" value="<?php echo $TPL_VAR["member"]["mb_name"]?>" id="od_name" required maxlength="20">
</label>
</section>
<?php if(!$GLOBALS["is_member"]){?>
<section class="col col-6">
<label for="od_pwd" class="label">비밀번호 : 영,숫자 3~20자 (주문서 조회시 필요)</label>
<label class="input">
<i class="icon-append fa fa-lock"></i>
<input type="password" name="od_pwd" id="od_pwd" required maxlength="20">
</label>
</section>
<?php }?>
</div>
<div class="margin-hr-5"></div>
<div class="row">
<section class="col col-4">
<label for="od_email" class="label">E-mail<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-envelope-o"></i>
<input type="text" name="od_email" value="<?php echo $TPL_VAR["member"]["mb_email"]?>" id="od_email" required size="35" maxlength="100">
</label>
</section>
<section class="col col-4">
<label for="od_tel" class="label">전화번호</label>
<label class="input">
<i class="icon-append fa fa-phone"></i>
<input type="text" name="od_tel" value="<?php echo $TPL_VAR["member"]["mb_tel"]?>" id="od_tel"  maxlength="20">
</label>
</section>
<section class="col col-4">
<label for="od_hp" class="label">핸드폰<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-tablet"></i>
<input type="text" name="od_hp" value="<?php echo $TPL_VAR["member"]["mb_hp"]?>" id="od_hp" required maxlength="20">
</label>
</section>
</div>
<div class="margin-hr-5"></div>
<div class="margin-bottom-10"></div>
<div class="row">
<section>
<div class="col col-2">
<label for="od_zip" class="sound_only">우편번호<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-question-circle"></i>
<input type="text" name="od_zip" id="od_zip" required size="5" maxlength="6" value="<?php echo $TPL_VAR["member"]["mb_zip1"]?><?php echo $TPL_VAR["member"]["mb_zip2"]?>">
<b class="tooltip tooltip-top-right">우편번호</b>
</label>
</div>
<div class="col col-2">
<button type="button" onclick="win_zip('forderform', 'od_zip', 'od_addr1', 'od_addr2', 'od_addr3', 'od_addr_jibeon');" class="btn-e btn-e-purple" style="padding-top:6px;padding-bottom:6px">주소 검색</button>
</div>
<div class="clearfix margin-bottom-10"></div>
<div class="col col-12">
<label class="input">
<input type="text" name="od_addr1" value="<?php echo $TPL_VAR["member"]["mb_addr1"]?>" id="od_addr1" required size="50">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 기본주소<strong class="sound_only"> 필수</strong></div>
</div>
<div class="clear"></div>
<div class="col col-6">
<label class="input">
<input type="text" name="od_addr2" value="<?php echo $TPL_VAR["member"]["mb_addr2"]?>" id="od_addr2" size="50">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 상세주소</div>
</div>
<div class="col col-6">
<label class="input">
<input type="text" name="od_addr3" value="<?php echo $TPL_VAR["member"]["mb_addr3"]?>" id="od_addr3" size="50" readonly="readonly">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 참고항목</div>
</div>
<input type="hidden" name="od_addr_jibeon" value="<?php echo $TPL_VAR["member"]["mb_addr_jibeon"]?>">
</section>
</div>
<?php if($TPL_VAR["default"]["de_hope_date_use"]){?>
<div class="margin-hr-5"></div>
<div class="row">
<div class="col col-6">
<label for="od_hope_date" class="label">희망배송일 선택</label>
<label class="input">
<i class="icon-append fa fa-calendar-o"></i>
<input type="text" name="od_hope_date" value="" id="od_hope_date" required size="11" maxlength="10" readonly="readonly">
</label>
<div class="note">위 희망배송일 이후로 배송 바랍니다.</div>
</div>
</div>
<?php }?>
</section>
<div class="headline margin-top-30"><h5><strong>받으시는 분</strong></h5></div>
<section class="sod-frm-taker">
<div class="row">
<div class="col col-8">
<label class="label">배송지선택</label>
<div class="inline-group">
<label for="ad_sel_addr_same" class="radio"><input type="radio" name="ad_sel_addr" value="same" id="ad_sel_addr_same"><i class="rounded-x"></i>주문자와 동일</label>
<?php if($GLOBALS["is_member"]){?>
<?php if($GLOBALS["ad_sel_addr"]){?>
<label for="ad_sel_addr_def" class="radio"><input type="radio" name="ad_sel_addr" value="<?php echo $GLOBALS["ad_sel_addr"]?>" id="ad_sel_addr_def"><i class="rounded-x"></i>기본배송지</label>
<?php }?>
<?php if($TPL__latest_addr_1){foreach($GLOBALS["latest_addr"] as $TPL_K1=>$TPL_V1){?>
<label for="ad_sel_addr_<?php echo $TPL_K1+ 1?>" class="radio"><input type="radio" name="ad_sel_addr" value="<?php echo $TPL_V1["val1"]?>" id="ad_sel_addr_<?php echo $TPL_K1+ 1?>"><i class="rounded-x"></i>최근배송지(<?php if($TPL_V1["ad_subject"]){?><?php echo $TPL_V1["ad_subject"]?><?php }else{?><?php echo $TPL_V1["ad_name"]?><?php }?>)</label>
<?php }}?>
<label for="od_sel_addr_new" class="radio"><input type="radio" name="ad_sel_addr" value="new" id="od_sel_addr_new"><i class="rounded-x"></i>신규배송지</label>
<?php }?>
</div>
</div>
<div class="col col-4 text-right">
<a href="<?php echo G5_SHOP_URL?>/orderaddress.php" id="order_address" class="btn-e btn-e-green">배송지목록</a>
</div>
</div>
<div class="margin-hr-5"></div>
<div class="row">
<section class="col col-6">
<label for="ad_subject" class="label">배송지명</label>
<label class="input">
<i class="icon-append fa fa-map-marker"></i>
<input type="text" name="ad_subject" id="ad_subject" maxlength="20">
</label>
</section>
<?php if($GLOBALS["is_member"]){?>
<section class="col col-6">
<label for="ad_default" class="label">배송지 설정</label>
<label class="checkbox">
<input type="checkbox" name="ad_default" id="ad_default" value="1"><i></i>기본배송지로 설정
</label>
</section>
<?php }?>
</div>
<div class="margin-hr-5"></div>
<div class="row">
<section class="col col-4">
<label for="od_b_name" class="label">이름<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-user"></i>
<input type="text" name="od_b_name" id="od_b_name" required maxlength="20">
</label>
</section>
<section class="col col-4">
<label for="od_b_tel" class="label">전화번호</label>
<label class="input">
<i class="icon-append fa fa-phone"></i>
<input type="text" name="od_b_tel" id="od_b_tel"  maxlength="20">
</label>
</section>
<section class="col col-4">
<label for="od_hp" class="label">핸드폰<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-tablet"></i>
<input type="text" name="od_b_hp" id="od_b_hp" required maxlength="20">
</label>
</section>
</div>
<div class="margin-hr-5"></div>
<div class="margin-bottom-10"></div>
<div class="row">
<div class="col col-2">
<label for="od_b_zip" class="sound_only">우편번호<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-question-circle"></i>
<input type="text" name="od_b_zip" id="od_b_zip" required size="5" maxlength="6">
<b class="tooltip tooltip-top-right">우편번호</b>
</label>
</div>
<div class="col col-2">
<button type="button" onclick="win_zip('forderform', 'od_b_zip', 'od_b_addr1', 'od_b_addr2', 'od_b_addr3', 'od_b_addr_jibeon');" class="btn-e btn-e-purple font-size-14" style="padding-top:6px;padding-bottom:6px">주소 검색</button>
</div>
<div class="clearfix margin-bottom-10"></div>
<div class="col col-12">
<label class="input">
<input type="text" name="od_b_addr1" id="od_b_addr1" required size="50">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 기본주소<strong class="sound_only"> 필수</strong></div>
</div>
<div class="clear"></div>
<div class="col col-6">
<label class="input">
<input type="text" name="od_b_addr2" id="od_b_addr2" size="50">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 상세주소</div>
</div>
<div class="col col-6">
<label class="input">
<input type="text" name="od_b_addr3" id="od_b_addr3" size="50" readonly="readonly">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 참고항목</div>
</div>
<input type="hidden" name="od_b_addr_jibeon" value="<?php echo $TPL_VAR["member"]["mb_addr_jibeon"]?>">
<div class="margin-hr-5"></div>
<div class="col col-12">
<label class="label" for="od_memo">전하실 말씀</label>
<label class="textarea">
<textarea rows="3" name="od_memo" id="od_memo"></textarea>
</label>
<div class="note">주문시 전하실 말씀을 적어주시기 바랍니다.</div>
</div>
</div>
</section>
<div class="headline margin-top-30"><h5><strong>결제 정보</strong></h5></div>
<section class="sod-frm-pay">
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb margin-bottom-10">
<div class="table-responsive">
<table class="table table-bordered">
<tbody>
<?php if($GLOBALS["oc_cnt"]> 0){?>
<tr>
<th>주문할인쿠폰</th>
<td>
<input type="hidden" name="od_cp_id" value="">
<button type="button" id="od_coupon_btn" class="btn-e btn-e-default btn-e-xs">쿠폰적용</button>
</td>
</tr>
<tr>
<th>주문할인금액</th>
<td><span id="od_cp_price">0</span> 원</td>
</tr>
<?php }?>
<?php if($GLOBALS["sc_cnt"]> 0){?>
<tr>
<th>배송비할인쿠폰</th>
<td>
<input type="hidden" name="sc_cp_id" value="">
<button type="button" id="sc_coupon_btn" class="btn-e btn-e-dark btn-e-xs">쿠폰적용</button>
</td>
</tr>
<tr>
<th>배송비할인금액</th>
<td><span id="sc_cp_price">0</span> 원</td>
</tr>
<?php }?>
<tr>
<th>총 주문금액</th>
<td><span id="od_tot_price"><?php echo number_format($GLOBALS["tot_price"])?></span> 원</td>
</tr>
<tr>
<th>추가배송비</th>
<td><span id="od_send_cost2">0</span> 원 (지역에 따라 추가되는 도선료 등의 배송비입니다.)</td>
</tr>
</tbody>
</table>
</div>
</div>
<?php if(!$TPL_VAR["default"]["de_card_point"]){?>
<div class="note margin-bottom-10"><strong><i class="fa fa-exclamation-circle"></i> Note: </strong>무통장입금 이외의 결제 수단으로 결제하시는 경우 포인트를 적립해드리지 않습니다.</div>
<?php }?>
<div class="sod-frm-paysel">
<?php if($GLOBALS["is_kakaopay_use"]||$TPL_VAR["default"]["de_bank_use"]||$TPL_VAR["default"]["de_vbank_use"]||$TPL_VAR["default"]["de_iche_use"]||$TPL_VAR["default"]["de_card_use"]||$TPL_VAR["default"]["de_hp_use"]||$TPL_VAR["default"]["de_easy_pay_use"]){?>
<fieldset>
<label class="label">결제방법 선택</label>
<?php }?>
<div class="inline-group">
<?php if($GLOBALS["is_kakaopay_use"]){?>
<label for="od_settle_kakaopay" class="radio"><input type="radio" id="od_settle_kakaopay" name="od_settle_case" value="KAKAOPAY"><i class="rounded-x"></i>KAKAOPAY</label>
<?php }?>
<?php if($TPL_VAR["default"]["de_bank_use"]){?>
<label for="od_settle_bank" class="radio"><input type="radio" id="od_settle_bank" name="od_settle_case" value="무통장"><i class="rounded-x"></i>무통장입금</label>
<?php }?>
<?php if($TPL_VAR["default"]["de_vbank_use"]){?>
<label for="od_settle_vbank" class="radio"><input type="radio" id="od_settle_vbank" name="od_settle_case" value="가상계좌"><i class="rounded-x"></i><?php echo $GLOBALS["escrow_title"]?>가상계좌</label>
<?php }?>
<?php if($TPL_VAR["default"]["de_iche_use"]){?>
<label for="od_settle_iche" class="radio"><input type="radio" id="od_settle_iche" name="od_settle_case" value="계좌이체"><i class="rounded-x"></i><?php echo $GLOBALS["escrow_title"]?>계좌이체</label>
<?php }?>
<?php if($TPL_VAR["default"]["de_hp_use"]){?>
<label for="od_settle_hp" class="radio"><input type="radio" id="od_settle_hp" name="od_settle_case" value="휴대폰"><i class="rounded-x"></i>휴대폰</label>
<?php }?>
<?php if($TPL_VAR["default"]["de_card_use"]){?>
<label for="od_settle_card" class="radio"><input type="radio" id="od_settle_card" name="od_settle_case" value="신용카드"><i class="rounded-x"></i>신용카드</label>
<?php }?>
<?php if($TPL_VAR["default"]["de_easy_pay_use"]){?>
<label for="od_settle_easy_pay" class="radio"><input type="radio" id="od_settle_easy_pay" name="od_settle_case" value="간편결제"><i class="rounded-x"></i><?php echo $GLOBALS["pg_easy_pay_name"]?></label>
<?php }?>
</div>
<div class="margin-hr-10"></div>
<?php if($GLOBALS["temp_point"]){?>
<p id="sod_frm_pt"><i class="fa fa-info-circle"></i> 보유포인트( <?php echo display_point($TPL_VAR["member"]["mb_point"])?> )중 <strong id="use_max_point">최대 <?php echo display_point($GLOBALS["temp_point"])?></strong>까지 사용 가능</p>
<div class="margin-hr-10"></div>
<div class="row">
<div class="col col-6">
<input type="hidden" name="max_temp_point" value="<?php echo $GLOBALS["temp_point"]?>">
<label for="od_temp_point" class="label">사용 포인트</label>
<label class="input">
<i class="icon-append fa fa-question-circle"></i>
<input type="text" name="od_temp_point" value="0" id="od_temp_point" size="10">
<b class="tooltip tooltip-top-right"><?php echo $GLOBALS["point_unit"]?>점 단위</b>
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> <?php echo $GLOBALS["point_unit"]?>점 단위로 입력하세요.</div>
<div class="clearfix"></div>
</div>
</div>
<?php }?>
<?php if($TPL_VAR["default"]["de_bank_use"]){?>
<div id="settle_bank" style="display:none">
<div class="row">
<div class="col col-6">
<label for="od_bank_account" class="label">입금할 계좌</label>
<?php if(count($GLOBALS["bank_str"])<= 1){?>
<h5 class="color-black margin-top-10" style="font-weight: bold;"><?php echo $GLOBALS["bank_account"]?></h5>
<input type="hidden" name="od_bank_account" value="<?php echo $GLOBALS["bank_account"]?>">
<?php }else{?>
<label class="select">
<select name="od_bank_account" id="od_bank_account" class="form-control">
<option value="">선택하십시오.</option>
<?php if($TPL__bank_account_1){foreach($GLOBALS["bank_account"] as $TPL_V1){?>
<option value="<?php echo $TPL_V1["bank"]?>"><?php echo $TPL_V1["bank"]?></option>
<?php }}?>
</select>
<i></i>
</label>
<?php }?>
</div>
<div class="col col-6">
<label for="od_deposit_name" class="label">입금자명</label>
<label class="input margin-bottom-10">
<i class="icon-append fa fa-user"></i>
<input type="text" name="od_deposit_name" id="od_deposit_name" size="10" maxlength="20">
</label>
</div>
</div>
</div>
<?php }?>
<?php if($GLOBALS["is_kakaopay_use"]||$TPL_VAR["default"]["de_bank_use"]||$TPL_VAR["default"]["de_vbank_use"]||$TPL_VAR["default"]["de_iche_use"]||$TPL_VAR["default"]["de_card_use"]||$TPL_VAR["default"]["de_hp_use"]||$TPL_VAR["default"]["de_easy_pay_use"]){?>
</fieldset>
<?php }?>
<?php if($GLOBALS["multi_settle"]== 0){?>
<p class="text-center"><i class="fa fa-exclamation-circle"></i> 결제할 방법이 없습니다.<br>운영자에게 알려주시면 감사하겠습니다.</p>
<?php }?>
</div>
</section>
<?php echo $GLOBALS["orderform3"]?>
<?php if($GLOBALS["is_kakaopay_use"]){?>
<?php echo $GLOBALS["kakaopay_orderform3"]?>
<?php }?>
</form>
<?php if($TPL_VAR["default"]["de_escrow_use"]){?>
<?php echo $GLOBALS["orderform4"]?>
<?php }?>
</div>
<script>
var zipcode = "";
$(function() {
var $cp_btn_el;
var $cp_row_el;
$(".cp_btn").click(function() {
$cp_btn_el = $(this);
$cp_row_el = $(this).closest("tr");
$("#cp_frm").remove();
var it_id = $cp_btn_el.closest("tr").find("input[name^=it_id]").val();
$.post(
"./orderitemcoupon.php",
{ it_id: it_id,  sw_direct: "<?php echo $GLOBALS["sw_direct"]?>" },
function(data) {
$cp_btn_el.after(data);
}
);
});
$(document).on("click", ".cp_apply", function() {
var $el = $(this).closest("tr");
var cp_id = $el.find("input[name='f_cp_id[]']").val();
var price = $el.find("input[name='f_cp_prc[]']").val();
var subj = $el.find("input[name='f_cp_subj[]']").val();
var sell_price;
if(parseInt(price) == 0) {
if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
return false;
}
}
// 이미 사용한 쿠폰이 있는지
var cp_dup = false;
var cp_dup_idx;
var $cp_dup_el;
$("input[name^=cp_id]").each(function(index) {
var id = $(this).val();
if(id == cp_id) {
cp_dup_idx = index;
cp_dup = true;
$cp_dup_el = $(this).closest("tr");;
return false;
}
});
if(cp_dup) {
var it_name = $("input[name='it_name["+cp_dup_idx+"]']").val();
if(!confirm(subj+ "쿠폰은 "+it_name+"에 사용되었습니다.\n"+it_name+"의 쿠폰을 취소한 후 적용하시겠습니까?")) {
return false;
} else {
coupon_cancel($cp_dup_el);
$("#cp_frm").remove();
$cp_dup_el.find(".cp_btn").text("적용").focus();
$cp_dup_el.find(".cp_cancel").remove();
}
}
var $s_el = $cp_row_el.find(".total_price");;
sell_price = parseInt($cp_row_el.find("input[name^=it_price]").val());
sell_price = sell_price - parseInt(price);
if(sell_price < 0) {
alert("쿠폰할인금액이 상품 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.");
return false;
}
$s_el.text(number_format(String(sell_price)));
$cp_row_el.find("input[name^=cp_id]").val(cp_id);
$cp_row_el.find("input[name^=cp_price]").val(price);
calculate_total_price();
$("#cp_frm").remove();
$cp_btn_el.text("변경").focus();
if(!$cp_row_el.find(".cp_cancel").size())
$cp_btn_el.after("<button type=\"button\" class=\"cp_cancel btn-e btn-e-red btn-e-xs\">취소</button>");
});
$(document).on("click", "#cp_close", function() {
$("#cp_frm").remove();
$cp_btn_el.focus();
});
$(document).on("click", ".cp_cancel", function() {
coupon_cancel($(this).closest("tr"));
calculate_total_price();
$("#cp_frm").remove();
$(this).closest("tr").find(".cp_btn").text("적용").focus();
$(this).remove();
});
$("#od_coupon_btn").click(function() {
$("#od_coupon_frm").remove();
var $this = $(this);
var price = parseInt($("input[name=org_od_price]").val()) - parseInt($("input[name=item_coupon]").val());
if(price <= 0) {
alert('상품금액이 0원이므로 쿠폰을 사용할 수 없습니다.');
return false;
}
$.post(
"./ordercoupon.php",
{ price: price },
function(data) {
$this.after(data);
}
);
});
$(document).on("click", ".od_cp_apply", function() {
var $el = $(this).closest("tr");
var cp_id = $el.find("input[name='o_cp_id[]']").val();
var price = parseInt($el.find("input[name='o_cp_prc[]']").val());
var subj = $el.find("input[name='o_cp_subj[]']").val();
var send_cost = $("input[name=od_send_cost]").val();
var item_coupon = parseInt($("input[name=item_coupon]").val());
var od_price = parseInt($("input[name=org_od_price]").val()) - item_coupon;
if(price == 0) {
if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
return false;
}
}
if(od_price - price <= 0) {
alert("쿠폰할인금액이 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.");
return false;
}
$("input[name=sc_cp_id]").val("");
$("#sc_coupon_btn").text("쿠폰적용");
$("#sc_coupon_cancel").remove();
$("input[name=od_price]").val(od_price - price);
$("input[name=od_cp_id]").val(cp_id);
$("input[name=od_coupon]").val(price);
$("input[name=od_send_coupon]").val(0);
$("#od_cp_price").text(number_format(String(price)));
$("#sc_cp_price").text(0);
calculate_order_price();
$("#od_coupon_frm").remove();
$("#od_coupon_btn").text("쿠폰변경").focus();
if(!$("#od_coupon_cancel").size())
$("#od_coupon_btn").after("<button type=\"button\" id=\"od_coupon_cancel\" class=\"btn-e btn-e-dark btn-e-xs\">쿠폰취소</button>");
});
$(document).on("click", "#od_coupon_close", function() {
$("#od_coupon_frm").remove();
$("#od_coupon_btn").focus();
});
$(document).on("click", "#od_coupon_cancel", function() {
var org_price = $("input[name=org_od_price]").val();
var item_coupon = parseInt($("input[name=item_coupon]").val());
$("input[name=od_price]").val(org_price - item_coupon);
$("input[name=sc_cp_id]").val("");
$("input[name=od_coupon]").val(0);
$("input[name=od_send_coupon]").val(0);
$("#od_cp_price").text(0);
$("#sc_cp_price").text(0);
calculate_order_price();
$("#od_coupon_frm").remove();
$("#od_coupon_btn").text("쿠폰적용").focus();
$(this).remove();
$("#sc_coupon_btn").text("쿠폰적용");
$("#sc_coupon_cancel").remove();
});
$("#sc_coupon_btn").click(function() {
$("#sc_coupon_frm").remove();
var $this = $(this);
var price = parseInt($("input[name=od_price]").val());
var send_cost = parseInt($("input[name=od_send_cost]").val());
$.post(
"./ordersendcostcoupon.php",
{ price: price, send_cost: send_cost },
function(data) {
$this.after(data);
}
);
});
$(document).on("click", ".sc_cp_apply", function() {
var $el = $(this).closest("tr");
var cp_id = $el.find("input[name='s_cp_id[]']").val();
var price = parseInt($el.find("input[name='s_cp_prc[]']").val());
var subj = $el.find("input[name='s_cp_subj[]']").val();
var send_cost = parseInt($("input[name=od_send_cost]").val());
if(parseInt(price) == 0) {
if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
return false;
}
}
$("input[name=sc_cp_id]").val(cp_id);
$("input[name=od_send_coupon]").val(price);
$("#sc_cp_price").text(number_format(String(price)));
calculate_order_price();
$("#sc_coupon_frm").remove();
$("#sc_coupon_btn").text("쿠폰변경").focus();
if(!$("#sc_coupon_cancel").size())
$("#sc_coupon_btn").after("<button type=\"button\" id=\"sc_coupon_cancel\" class=\"btn-e btn-e-red btn-e-xs\">쿠폰취소</button>");
});
$(document).on("click", "#sc_coupon_close", function() {
$("#sc_coupon_frm").remove();
$("#sc_coupon_btn").focus();
});
$(document).on("click", "#sc_coupon_cancel", function() {
$("input[name=od_send_coupon]").val(0);
$("#sc_cp_price").text(0);
calculate_order_price();
$("#sc_coupon_frm").remove();
$("#sc_coupon_btn").text("쿠폰적용").focus();
$(this).remove();
});
$("#od_b_addr2").focus(function() {
var zip = $("#od_b_zip").val().replace(/[^0-9]/g, "");
if(zip == "")
return false;
var code = String(zip);
if(zipcode == code)
return false;
zipcode = code;
calculate_sendcost(code);
});
$("#od_settle_bank").on("click", function() {
$("[name=od_deposit_name]").val( $("[name=od_name]").val() );
$("#settle_bank").show();
});
$("#od_settle_iche,#od_settle_card,#od_settle_vbank,#od_settle_hp,#od_settle_easy_pay,#od_settle_kakaopay").bind("click", function() {
$("#settle_bank").hide();
});
// 배송지선택
$("input[name=ad_sel_addr]").on("click", function() {
var addr = $(this).val().split(String.fromCharCode(30));
if (addr[0] == "same") {
gumae2baesong();
} else {
if(addr[0] == "new") {
for(i=0; i<10; i++) {
addr[i] = "";
}
}
var f = document.forderform;
f.od_b_name.value        = addr[0];
f.od_b_tel.value         = addr[1];
f.od_b_hp.value          = addr[2];
f.od_b_zip.value         = addr[3] + addr[4];
f.od_b_addr1.value       = addr[5];
f.od_b_addr2.value       = addr[6];
f.od_b_addr3.value       = addr[7];
f.od_b_addr_jibeon.value = addr[8];
f.ad_subject.value       = addr[9];
var zip1 = addr[3].replace(/[^0-9]/g, "");
var zip2 = addr[4].replace(/[^0-9]/g, "");
var code = String(zip1) + String(zip2);
if(zipcode != code) {
calculate_sendcost(code);
}
}
});
// 배송지목록
$("#order_address").on("click", function() {
var url = this.href;
window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
return false;
});
});
function coupon_cancel($el)
{
var $dup_sell_el = $el.find(".total_price");
var $dup_price_el = $el.find("input[name^=cp_price]");
var org_sell_price = $el.find("input[name^=it_price]").val();
$dup_sell_el.text(number_format(String(org_sell_price)));
$dup_price_el.val(0);
$el.find("input[name^=cp_id]").val("");
}
function calculate_total_price()
{
var $it_prc = $("input[name^=it_price]");
var $cp_prc = $("input[name^=cp_price]");
var tot_sell_price = sell_price = tot_cp_price = 0;
var it_price, cp_price, it_notax;
var tot_mny = comm_tax_mny = comm_vat_mny = comm_free_mny = tax_mny = vat_mny = 0;
var send_cost = parseInt($("input[name=od_send_cost]").val());
$it_prc.each(function(index) {
it_price = parseInt($(this).val());
cp_price = parseInt($cp_prc.eq(index).val());
sell_price += it_price;
tot_cp_price += cp_price;
});
tot_sell_price = sell_price - tot_cp_price + send_cost;
$("#ct_tot_coupon").text(number_format(String(tot_cp_price))+" 원");
$("#ct_tot_price").text(number_format(String(tot_sell_price))+" 원");
$("input[name=good_mny]").val(tot_sell_price);
$("input[name=od_price]").val(sell_price - tot_cp_price);
$("input[name=item_coupon]").val(tot_cp_price);
$("input[name=od_coupon]").val(0);
$("input[name=od_send_coupon]").val(0);
<?php if($GLOBALS["oc_cnt"]){?>
$("input[name=od_cp_id]").val("");
$("#od_cp_price").text(0);
if($("#od_coupon_cancel").size()) {
$("#od_coupon_btn").text("쿠폰적용");
$("#od_coupon_cancel").remove();
}
<?php }?>
<?php if($GLOBALS["sc_cnt"]> 0){?>
$("input[name=sc_cp_id]").val("");
$("#sc_cp_price").text(0);
if($("#sc_coupon_cancel").size()) {
$("#sc_coupon_btn").text("쿠폰적용");
$("#sc_coupon_cancel").remove();
}
<?php }?>
$("input[name=od_temp_point]").val(0);
<?php if($GLOBALS["temp_point"]> 0&&$GLOBALS["is_member"]){?>
calculate_temp_point();
<?php }?>
calculate_order_price();
}
function calculate_order_price()
{
var sell_price = parseInt($("input[name=od_price]").val());
var send_cost = parseInt($("input[name=od_send_cost]").val());
var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
var send_coupon = parseInt($("input[name=od_send_coupon]").val());
var tot_price = sell_price + send_cost + send_cost2 - send_coupon;
$("input[name=good_mny]").val(tot_price);
$("#od_tot_price").text(number_format(String(tot_price)));
<?php if($GLOBALS["temp_point"]> 0&&$GLOBALS["is_member"]){?>
calculate_temp_point();
<?php }?>
}
function calculate_temp_point()
{
var sell_price = parseInt($("input[name=od_price]").val());
var mb_point = parseInt(<?php echo $TPL_VAR["member"]["mb_point"]?>);
var max_point = parseInt(<?php echo $TPL_VAR["default"]["de_settle_max_point"]?>);
var point_unit = parseInt(<?php echo $TPL_VAR["default"]["de_settle_point_unit"]?>);
var temp_point = max_point;
if(temp_point > sell_price)
temp_point = sell_price;
if(temp_point > mb_point)
temp_point = mb_point;
temp_point = parseInt(temp_point / point_unit) * point_unit;
$("#use_max_point").text("최대 "+number_format(String(temp_point))+"점");
$("input[name=max_temp_point]").val(temp_point);
}
function calculate_sendcost(code)
{
$.post(
"./ordersendcost.php",
{ zipcode: code },
function(data) {
$("input[name=od_send_cost2]").val(data);
$("#od_send_cost2").text(number_format(String(data)));
zipcode = code;
calculate_order_price();
}
);
}
function calculate_tax()
{
var $it_prc = $("input[name^=it_price]");
var $cp_prc = $("input[name^=cp_price]");
var sell_price = tot_cp_price = 0;
var it_price, cp_price, it_notax;
var tot_mny = comm_free_mny = tax_mny = vat_mny = 0;
var send_cost = parseInt($("input[name=od_send_cost]").val());
var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
var od_coupon = parseInt($("input[name=od_coupon]").val());
var send_coupon = parseInt($("input[name=od_send_coupon]").val());
var temp_point = 0;
$it_prc.each(function(index) {
it_price = parseInt($(this).val());
cp_price = parseInt($cp_prc.eq(index).val());
sell_price += it_price;
tot_cp_price += cp_price;
it_notax = $("input[name^=it_notax]").eq(index).val();
if(it_notax == "1") {
comm_free_mny += (it_price - cp_price);
} else {
tot_mny += (it_price - cp_price);
}
});
if($("input[name=od_temp_point]").size())
temp_point = parseInt($("input[name=od_temp_point]").val());
tot_mny += (send_cost + send_cost2 - od_coupon - send_coupon - temp_point);
if(tot_mny < 0) {
comm_free_mny = comm_free_mny + tot_mny;
tot_mny = 0;
}
tax_mny = Math.round(tot_mny / 1.1);
vat_mny = tot_mny - tax_mny;
$("input[name=comm_tax_mny]").val(tax_mny);
$("input[name=comm_vat_mny]").val(vat_mny);
$("input[name=comm_free_mny]").val(comm_free_mny);
}
function forderform_check(f)
{
// 재고체크
var stock_msg = order_stock_check();
if(stock_msg != "") {
alert(stock_msg);
return false;
}
errmsg = "";
errfld = "";
var deffld = "";
check_field(f.od_name, "주문하시는 분 이름을 입력하십시오.");
if (typeof(f.od_pwd) != 'undefined')
{
clear_field(f.od_pwd);
if( (f.od_pwd.value.length<3) || (f.od_pwd.value.search(/([^A-Za-z0-9]+)/)!=-1) )
error_field(f.od_pwd, "회원이 아니신 경우 주문서 조회시 필요한 비밀번호를 3자리 이상 입력해 주십시오.");
}
/*check_field(f.od_tel, "주문하시는 분 전화번호를 입력하십시오.");*/
check_field(f.od_hp, "주문하시는 분 핸드폰 번호를 입력하십시오.");
check_field(f.od_addr1, "주소검색을 이용하여 주문하시는 분 주소를 입력하십시오.");
//check_field(f.od_addr2, " 주문하시는 분의 상세주소를 입력하십시오.");
check_field(f.od_zip, "");
clear_field(f.od_email);
if(f.od_email.value=='' || f.od_email.value.search(/(\S+)@(\S+)\.(\S+)/) == -1)
error_field(f.od_email, "E-mail을 바르게 입력해 주십시오.");
if (typeof(f.od_hope_date) != "undefined")
{
clear_field(f.od_hope_date);
if (!f.od_hope_date.value)
error_field(f.od_hope_date, "희망배송일을 선택하여 주십시오.");
}
check_field(f.od_b_name, "받으시는 분 이름을 입력하십시오.");
/*check_field(f.od_b_tel, "받으시는 분 전화번호를 입력하십시오.");*/
check_field(f.od_b_hp, "받으시는 분 핸드폰 번호를 입력하십시오.");
check_field(f.od_b_addr1, "주소검색을 이용하여 받으시는 분 주소를 입력하십시오.");
//check_field(f.od_b_addr2, "받으시는 분의 상세주소를 입력하십시오.");
check_field(f.od_b_zip, "");
var od_settle_bank = document.getElementById("od_settle_bank");
if (od_settle_bank) {
if (od_settle_bank.checked) {
check_field(f.od_bank_account, "계좌번호를 선택하세요.");
check_field(f.od_deposit_name, "입금자명을 입력하세요.");
}
}
// 배송비를 받지 않거나 더 받는 경우 아래식에 + 또는 - 로 대입
f.od_send_cost.value = parseInt(f.od_send_cost.value);
if (errmsg)
{
alert(errmsg);
errfld.focus();
return false;
}
var settle_case = document.getElementsByName("od_settle_case");
var settle_check = false;
var settle_method = "";
for (i=0; i<settle_case.length; i++)
{
if (settle_case[i].checked)
{
settle_check = true;
settle_method = settle_case[i].value;
break;
}
}
if (!settle_check)
{
alert("결제방식을 선택하십시오.");
return false;
}
var od_price = parseInt(f.od_price.value);
var send_cost = parseInt(f.od_send_cost.value);
var send_cost2 = parseInt(f.od_send_cost2.value);
var send_coupon = parseInt(f.od_send_coupon.value);
var max_point = 0;
if (typeof(f.max_temp_point) != "undefined")
max_point  = parseInt(f.max_temp_point.value);
var temp_point = 0;
if (typeof(f.od_temp_point) != "undefined") {
if (f.od_temp_point.value)
{
var point_unit = parseInt(<?php echo $TPL_VAR["default"]["de_settle_point_unit"]?>);
temp_point = parseInt(f.od_temp_point.value);
if (temp_point < 0) {
alert("포인트를 0 이상 입력하세요.");
f.od_temp_point.select();
return false;
}
if (temp_point > od_price) {
alert("상품 주문금액(배송비 제외) 보다 많이 포인트결제할 수 없습니다.");
f.od_temp_point.select();
return false;
}
if (temp_point > parseInt(<?php echo $TPL_VAR["member"]["mb_point"]?>)) {
alert("회원님의 포인트보다 많이 결제할 수 없습니다.");
f.od_temp_point.select();
return false;
}
if (temp_point > max_point) {
alert(max_point + "점 이상 결제할 수 없습니다.");
f.od_temp_point.select();
return false;
}
if (parseInt(parseInt(temp_point / point_unit) * point_unit) != temp_point) {
alert("포인트를 "+String(point_unit)+"점 단위로 입력하세요.");
f.od_temp_point.select();
return false;
}
// pg 결제 금액에서 포인트 금액 차감
if(settle_method != "무통장") {
f.good_mny.value = od_price + send_cost + send_cost2 - send_coupon - temp_point;
}
}
}
var tot_price = od_price + send_cost + send_cost2 - send_coupon - temp_point;
if (document.getElementById("od_settle_iche")) {
if (document.getElementById("od_settle_iche").checked) {
if (tot_price < 150) {
alert("계좌이체는 150원 이상 결제가 가능합니다.");
return false;
}
}
}
if (document.getElementById("od_settle_card")) {
if (document.getElementById("od_settle_card").checked) {
if (tot_price < 1000) {
alert("신용카드는 1000원 이상 결제가 가능합니다.");
return false;
}
}
}
if (document.getElementById("od_settle_hp")) {
if (document.getElementById("od_settle_hp").checked) {
if (tot_price < 350) {
alert("휴대폰은 350원 이상 결제가 가능합니다.");
return false;
}
}
}
<?php if($TPL_VAR["default"]["de_tax_flag_use"]){?>
calculate_tax();
<?php }?>
// 카카오페이 지불
if(settle_method == "KAKAOPAY") {
<?php if($TPL_VAR["default"]["de_tax_flag_use"]){?>
f.SupplyAmt.value = parseInt(f.comm_tax_mny.value) + parseInt(f.comm_free_mny.value);
f.GoodsVat.value  = parseInt(f.comm_vat_mny.value);
<?php }?>
getTxnId(f);
return false;
}
// pay_method 설정
<?php if($TPL_VAR["default"]["de_pg_service"]=='kcp'){?>
f.site_cd.value = f.def_site_cd.value;
f.payco_direct.value = "";
switch(settle_method)
{
case "계좌이체":
f.pay_method.value 	 = "010000000000";
break;
case "가상계좌":
f.pay_method.value 	 = "001000000000";
break;
case "휴대폰":
f.pay_method.value 	 = "000010000000";
break;
case "신용카드":
f.pay_method.value 	 = "100000000000";
break;
case "간편결제":
<?php if($TPL_VAR["default"]["de_card_test"]){?>
f.site_cd.value		 = "S6729";
<?php }?>
f.pay_method.value	 = "100000000000";
f.payco_direct.value = "Y";
break;
default:
f.pay_method.value = "무통장";
break;
}
<?php }elseif($TPL_VAR["default"]["de_pg_service"]=='lg'){?>
f.LGD_EASYPAY_ONLY.value = "";
if(typeof f.LGD_CUSTOM_USABLEPAY === "undefined") {
var input = document.createElement("input");
input.setAttribute("type", "hidden");
input.setAttribute("name", "LGD_CUSTOM_USABLEPAY");
input.setAttribute("value", "");
f.LGD_EASYPAY_ONLY.parentNode.insertBefore(input, f.LGD_EASYPAY_ONLY);
}
switch(settle_method)
{
case "계좌이체":
f.LGD_CUSTOM_FIRSTPAY.value = "SC0030";
f.LGD_CUSTOM_USABLEPAY.value = "SC0030";
break;
case "가상계좌":
f.LGD_CUSTOM_FIRSTPAY.value = "SC0040";
f.LGD_CUSTOM_USABLEPAY.value = "SC0040";
break;
case "휴대폰":
f.LGD_CUSTOM_FIRSTPAY.value = "SC0060";
f.LGD_CUSTOM_USABLEPAY.value = "SC0060";
break;
case "신용카드":
f.LGD_CUSTOM_FIRSTPAY.value = "SC0010";
f.LGD_CUSTOM_USABLEPAY.value = "SC0010";
break;
case "간편결제":
var elm = f.LGD_CUSTOM_USABLEPAY;
if(elm.parentNode)
elm.parentNode.removeChild(elm);
f.LGD_EASYPAY_ONLY.value = "PAYNOW";
break;
default:
f.LGD_CUSTOM_FIRSTPAY.value = "무통장";
break;
}
<?php }elseif($TPL_VAR["default"]["de_pg_service"]=='inicis'){?>
switch(settle_method)
{
case "계좌이체":
f.gopaymethod.value = "DirectBank";
break;
case "가상계좌":
f.gopaymethod.value = "VBank";
break;
case "휴대폰":
f.gopaymethod.value = "HPP";
break;
case "신용카드":
f.gopaymethod.value = "Card";
f.acceptmethod.value = f.acceptmethod.value.replace(":useescrow", "");
break;
case "간편결제":
f.gopaymethod.value = "Kpay";
break;
default:
f.gopaymethod.value = "무통장";
break;
}
<?php }?>
// 결제정보설정
<?php if($TPL_VAR["default"]["de_pg_service"]=='kcp'){?>
f.buyr_name.value = f.od_name.value;
f.buyr_mail.value = f.od_email.value;
f.buyr_tel1.value = f.od_tel.value;
f.buyr_tel2.value = f.od_hp.value;
f.rcvr_name.value = f.od_b_name.value;
f.rcvr_tel1.value = f.od_b_tel.value;
f.rcvr_tel2.value = f.od_b_hp.value;
f.rcvr_mail.value = f.od_email.value;
f.rcvr_zipx.value = f.od_b_zip.value;
f.rcvr_add1.value = f.od_b_addr1.value;
f.rcvr_add2.value = f.od_b_addr2.value;
if(f.pay_method.value != "무통장") {
jsf__pay( f );
} else {
f.submit();
}
<?php }?>
<?php if($TPL_VAR["default"]["de_pg_service"]=='lg'){?>
f.LGD_BUYER.value = f.od_name.value;
f.LGD_BUYEREMAIL.value = f.od_email.value;
f.LGD_BUYERPHONE.value = f.od_hp.value;
f.LGD_AMOUNT.value = f.good_mny.value;
f.LGD_RECEIVER.value = f.od_b_name.value;
f.LGD_RECEIVERPHONE.value = f.od_b_hp.value;
<?php if($TPL_VAR["default"]["de_escrow_use"]){?>
f.LGD_ESCROW_ZIPCODE.value = f.od_b_zip.value;
f.LGD_ESCROW_ADDRESS1.value = f.od_b_addr1.value;
f.LGD_ESCROW_ADDRESS2.value = f.od_b_addr2.value;
f.LGD_ESCROW_BUYERPHONE.value = f.od_hp.value;
<?php }?>
<?php if($TPL_VAR["default"]["de_tax_flag_use"]){?>
f.LGD_TAXFREEAMOUNT.value = f.comm_free_mny.value;
<?php }?>
if(f.LGD_CUSTOM_FIRSTPAY.value != "무통장") {
launchCrossPlatform(f);
} else {
f.submit();
}
<?php }?>
<?php if($TPL_VAR["default"]["de_pg_service"]=='inicis'){?>
f.price.value       = f.good_mny.value;
<?php if($TPL_VAR["default"]["de_tax_flag_use"]){?>
f.tax.value         = f.comm_vat_mny.value;
f.taxfree.value     = f.comm_free_mny.value;
<?php }?>
f.buyername.value   = f.od_name.value;
f.buyeremail.value  = f.od_email.value;
f.buyertel.value    = f.od_hp.value ? f.od_hp.value : f.od_tel.value;
f.recvname.value    = f.od_b_name.value;
f.recvtel.value     = f.od_b_hp.value ? f.od_b_hp.value : f.od_b_tel.value;
f.recvpostnum.value = f.od_b_zip.value;
f.recvaddr.value    = f.od_b_addr1.value + " " +f.od_b_addr2.value;
if(f.gopaymethod.value != "무통장") {
// 주문정보 임시저장
var order_data = $(f).serialize();
var save_result = "";
$.ajax({
type: "POST",
data: order_data,
url: g5_url+"/shop/ajax.orderdatasave.php",
cache: false,
async: false,
success: function(data) {
save_result = data;
}
});
if(save_result) {
alert(save_result);
return false;
}
if(!make_signature(f))
return false;
paybtn(f);
} else {
f.submit();
}
<?php }?>
}
// 구매자 정보와 동일합니다.
function gumae2baesong() {
var f = document.forderform;
f.od_b_name.value = f.od_name.value;
f.od_b_tel.value  = f.od_tel.value;
f.od_b_hp.value   = f.od_hp.value;
f.od_b_zip.value  = f.od_zip.value;
f.od_b_addr1.value = f.od_addr1.value;
f.od_b_addr2.value = f.od_addr2.value;
f.od_b_addr3.value = f.od_addr3.value;
f.od_b_addr_jibeon.value = f.od_addr_jibeon.value;
calculate_sendcost(String(f.od_b_zip.value));
}
<?php if($TPL_VAR["default"]["de_hope_date_use"]){?>
$(function(){
$("#od_hope_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", minDate: "+<?php echo $TPL_VAR["default"]["de_hope_date_after"]* 1?>d;", maxDate: "+<?php echo $TPL_VAR["default"]["de_hope_date_after"]* 1+ 6?>d;" });
});
<?php }?>
</script>