<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/list.10.skin.html 000007177 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; // ?>
<div class="product-list-10 row">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<?php if(!G5_IS_MOBILE){?>
<div class="col-sm-4">
<?php }else{?>
<div class="col-xs-6">
<?php }?>
<div class="item-list-10">
<div class="product-img">
<?php if($TPL_VAR["href"]){?>
<a href="<?php echo $TPL_V1["href"]?>">
<?php }?>
<?php echo $TPL_V1["it_image"]?>
<?php if($TPL_VAR["href"]){?>
</a>
<?php }?>
<!-- <div class="product-sns">
<ul class="list-inline">
&lt;!&ndash;<?php if($TPL_VAR["view_sns"]){?>&ndash;&gt;
<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $TPL_V1["sns_url"]?>&amp;p=<?php echo $TPL_V1["sns_title"]?>" target="_blank" class="facebook-icon"><i class="fa fa-facebook"></i></a></li>
<li><a href="https://twitter.com/share?url=<?php echo $TPL_V1["sns_url"]?>&amp;text=<?php echo $TPL_V1["sns_title"]?>" target="_blank" class="twitter-icon"><i class="fa fa-twitter"></i></a></li>
<li><a href="https://plus.google.com/share?url=<?php echo $TPL_V1["sns_url"]?>" arget="_blank" class="google-icon"><i class="fa fa-google"></i></a></li>
&lt;!&ndash;<?php }?>&ndash;&gt;
<li><a href="javascript:item_wish_for_list('<?php echo $TPL_V1["it_id"]?>');" class="wish-icon"><i class="fa fa-heart"></i></a></li>
</ul>
</div>-->
<?php if($TPL_VAR["view_it_icon"]){?>
<?php echo $TPL_V1["it_icon"]?>
<?php }?>
</div>
<div class="product-description">
<div class="product-description-in">
<?php if($TPL_VAR["href"]){?>
<h4 class="product-name ellipsis">
<a href="<?php echo $TPL_V1["href"]?>">
<?php }?>
<?php if($TPL_VAR["view_it_name"]){?><?php echo stripslashes($TPL_V1["it_name"])?><?php }?>
<?php if($TPL_VAR["href"]){?>
</a>
</h4>
<?php }?>
<?php if($TPL_VAR["view_it_cust_price"]||$TPL_VAR["view_it_price"]){?>
<div class="product-price">
<?php if($TPL_VAR["view_it_price"]){?>
<span class="title-price">₩ <?php echo $TPL_V1["it_tel_inq"]?></span>
<?php }?>
<?php if($TPL_VAR["view_it_cust_price"]&&$TPL_V1["it_cust_price"]){?>
<span class="title-price line-through">₩ <?php echo $TPL_V1["it_cust_price"]?></span>
<?php }?>
</div>
<?php }?>
<?php if($TPL_VAR["view_it_id"]){?>
<span class="type text-uppercase"><?php echo stripslashes($TPL_V1["it_id"])?></span>
<?php }?>
<?php if( 0){?>
<?php if($TPL_VAR["view_it_basic"]&&$TPL_V1["it_basic"]){?>
<span class="type"><?php echo stripslashes($TPL_V1["it_basic"])?></span>
<?php }?>
<?php }?>
</div>
</div>
<div class="product-description-bottom">
<a class="pull-left font-size-12" href="<?php echo G5_SHOP_URL?>/itemuselist.php?sfl=a.it_id&stx=<?php echo $TPL_V1["it_id"]?>">리뷰보기</a>
<ul class="list-inline product-ratings pull-right">
<li><i class="rating<?php if($TPL_V1["star_score"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
</ul>
<div class="clearfix"></div>
</div>
</div>
</div>
<?php if(!G5_IS_MOBILE){?>
<?php if($TPL_K1% 3== 0){?>
<div class="clearfix"></div>
<?php }?>
<?php }else{?>
<?php if($TPL_K1% 2== 0){?>
<div class="clearfix"></div>
<?php }?>
<?php }?>
<?php }}else{?>
<p class="text-center">등록된 상품이 없습니다.</p>
<?php }?>
</div>
<style>
.product-list-10.row {margin-left:-10px;margin-right:-10px}
.product-list-10 .col-sm-4 {padding-left:10px;padding-right:10px}
.product-list-10 .item-list-10 {position:relative;border:1px solid #d5d5d5;margin-bottom:20px;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out}
.product-list-10 .item-e1:hover {border:1px solid #0085DA}
.product-list-10 .product-img {position:relative;padding:10px}
.product-list-10 .item-e1:hover .product-img {background:#f8f8f8}
.product-list-10 .product-img img {width:100%;display:block;max-width:100%;height:auto}
.product-list-10 .product-sns {left:0;right:0;top:50%;z-index:1;width:100%;color:#555;border:none;padding:7px 0;font-size:12px;margin-top:-20px;text-align:center;position:absolute;visibility:hidden;text-transform:uppercase;background:#fff;-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=95)";filter:alpha(opacity=95);opacity:0.95}
.product-list-10 .product-img:hover .product-sns {visibility:visible}
.product-list-10 .product-sns ul {margin:0}
.product-list-10 .product-sns ul li {padding-left:1px;padding-right:1px}
.product-list-10 .product-sns a {display:inline-block;width:24px;height:24px;line-height:24px;font-size:12px;background:#fff;border:1px solid #d5d5d5}
.product-list-10 .product-sns a.facebook-icon {color:#4862A3}
.product-list-10 .product-sns a.twitter-icon {color:#55ACEE}
.product-list-10 .product-sns a.google-icon {color:#D73D32}
.product-list-10 .product-sns a.wish-icon {color:#FF9400}
.product-list-10 .product-sns a.facebook-icon:hover {background:#4862A3;color:#fff;border:1px solid #4862A3}
.product-list-10 .product-sns a.twitter-icon:hover {background:#55ACEE;color:#fff;border:1px solid #55ACEE}
.product-list-10 .product-sns a.google-icon:hover {background:#D73D32;color:#fff;border:1px solid #D73D32}
.product-list-10 .product-sns a.wish-icon:hover {background:#FF9400;color:#fff;border:1px solid #FF9400}
.product-list-10 .item-e1:hover .product-description-in {background:#f8f8f8}
.product-list-10 .product-description .product-description-in {position:relative;overflow:hidden;padding:0 10px 10px}
.product-list-10 .product-description .product-name {margin:0;margin-bottom:5px}
.product-list-10 .product-description .product-name a {font-size:14px;font-weight:bold;color:#000}
.product-list-10 .product-description .product-name a:hover {color:#DE2600}
.product-list-10 .product-description .title-price {font-size:14px;font-weight:bold;color:#ae0000}
.product-list-10 .product-description .line-through {color:#b5b5b5;text-decoration:line-through;margin-left:7px;font-weight:normal}
.product-list-10 .product-description .type {color:#757575;display:block;font-size:12px;margin-top:8px}
.product-list-10 .product-description-bottom {position:relative;overflow:hidden;padding:7px 10px;border-top:1px solid #d5d5d5}
</style>
<?php if(G5_IS_MOBILE){?>
<style>
.product-list-10.row {margin-left:-3px;margin-right:-3px}
.product-list-10 .col-xs-6 {padding-left:3px;padding-right:3px}
.product-list-10 .product-img {padding:6px}
.product-list-10 .product-description .product-description-in {padding:0 6px 10px}
.product-list-10 .product-description-bottom {padding:7px 6px}
</style>
<?php }?>