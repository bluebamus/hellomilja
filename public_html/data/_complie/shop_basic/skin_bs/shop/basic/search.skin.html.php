<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/search.skin.html 000007135 */  $this->include_("eb_paging");
$TPL_slist_1=empty($TPL_VAR["slist"])||!is_array($TPL_VAR["slist"])?0:count($TPL_VAR["slist"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
.shop-search .shop-search-form {position:relative;padding:10px;border:1px solid #b5b5b5;background:#fbfbfb;margin-bottom:30px}
.shop-search .tab-e2 .nav-tabs li a {background:#f5f5f5}
.shop-search .tab-e2 .nav-tabs li a:hover {background:#e5e5e5;color:#000}
.shop-search .tab-e2 .nav-tabs li.active a {border:1px solid #000;border-top:1px solid #FF2900;background:#fff;z-index:1}
.shop-search .tab-e2 .tab-bottom-line {position:relative;height:1px;background:#000}
@media (max-width: 767px){
.shop-search .tab-e2 .nav-tabs {border:1px solid #b5b5b5;padding:10px 10px 5px;background:#fafafa}
.shop-search .tab-e2 .nav-tabs li a {background:#e5e5e5;color:#000;font-size:11px;padding:3px 7px;margin-bottom:5px}
.shop-search .tab-e2 .nav-tabs li.active a {background:#1C1C26;color:#fff;border:0}
.shop-search .tab-e2 .tab-bottom-line {display:none}
}
.shop-search .shop-search-category {position:relative;padding:10px;border:1px dotted #c5c5c5;background:#fbfbfb;margin-bottom:20px}
.shop-search .shop-search-category ul {margin-bottom:0}
.shop-search .shop-search-category ul li a {background:#e5e5e5;color:#000;padding:3px 7px;font-size:11px}
</style>
<?php if($GLOBALS["is_admin"]){?>
<div class="text-right">
<a href="<?php echo G5_ADMIN_URL?>/shop_admin/configform.php#anc_scf_etc'.'" class="btn-e btn-e-purple margin-bottom-10">검색 설정</a>
</div>
<?php }?>
<div class="shop-search">
<div class="shop-search-form">
<form name="frmdetailsearch" class="eyoom-form">
<input type="hidden" name="qsort" id="qsort" value="<?php echo $GLOBALS["qsort"]?>">
<input type="hidden" name="qorder" id="qorder" value="<?php echo $GLOBALS["qorder"]?>">
<input type="hidden" name="qcaid" id="qcaid" value="<?php echo $GLOBALS["qcaid"]?>">
<section>
<label class="label">검색범위</label>
<div class="inline-group">
<label for="ssch_qname" class="checkbox">
<input type="checkbox" name="qname" id="ssch_qname" value="1" <?php if($GLOBALS["qname_check"]){?>checked="checked"<?php }?>><i></i>상품명
</label>
<label for="ssch_qexplan" class="checkbox">
<input type="checkbox" name="qexplan" id="ssch_qexplan" value="1" <?php if($GLOBALS["qexplan_check"]){?>checked="checked"<?php }?>><i></i>상품설명
</label>
<label for="ssch_qexplan" class="checkbox">
<input type="checkbox" name="qexplan" id="ssch_qexplan" value="1" <?php if($GLOBALS["qexplan_check"]){?>checked="checked"<?php }?>><i></i>상품코드
</label>
</div>
</section>
<div class="margin-hr-10"></div>
<section class="row">
<div class="col col-3">
<label for="ssch_qfrom" class="label">최소 가격</label>
<label class="input">
<i class="icon-append fa fa-question-circle"></i>
<input type="text" name="qfrom" value="<?php echo $GLOBALS["qfrom"]?>" id="ssch_qfrom" size="10">
<b class="tooltip tooltip-top-right">최소가격(원)</b>
</label>
</div>
<div class="col col-3">
<label for="ssch_qto" class="label">최대 가격</label>
<label class="input">
<i class="icon-append fa fa-question-circle"></i>
<input type="text" name="qto" value="<?php echo $GLOBALS["qto"]?>" id="ssch_qto" size="10">
<b class="tooltip tooltip-top-right">최대가격(원)</b>
</label>
</div>
<div class="col col-6">
<label for="ssch_q" class="ssch_lbl label">검색어</label>
<div class="input-group">
<label class="input">
<input type="text" name="q" value="<?php echo $GLOBALS["q"]?>" id="ssch_q" class="form-control" size="40" maxlength="30">
</label>
<span class="input-group-btn">
<input class="btn btn-default btn-e-group" type="submit" value="검색">
</span>
</div>
</div>
<div class="clearfix"></div>
</section>
<div class="margin-hr-10"></div>
<section class="alert alert-warning padding-all-10">
<span>
상세검색을 선택하지 않거나, 상품가격을 입력하지 않으면 전체에서 검색합니다.<br>
검색어는 최대 30글자까지, 여러개의 검색어를 공백으로 구분하여 입력 할수 있습니다.
</span>
</section>
<div class="margin-hr-10"></div>
</form>
<section class="text-right">
검색 결과 <b><?php echo $GLOBALS["total_count"]?></b>건
</section>
</div>
<a name="scl"></a>
<section class="tab-e2 margin-bottom-20">
<ul class="nav nav-tabs">
<li <?php if($_GET["qsort"]=='it_sum_qty'&&$_GET["qorder"]=='desc'){?>class="active"<?php }?>>
<a href="#scl" onclick="set_sort('it_sum_qty', 'desc'); return false;">판매많은순</a>
</li>
<li <?php if($_GET["qsort"]=='it_price'&&$_GET["qorder"]=='asc'){?>class="active"<?php }?>>
<a href="#scl" onclick="set_sort('it_price', 'asc'); return false;">낮은가격순</a>
</li>
<li <?php if($_GET["qsort"]=='it_price'&&$_GET["qorder"]=='desc'){?>class="active"<?php }?>>
<a href="#scl" onclick="set_sort('it_price', 'desc'); return false;">높은가격순</a>
</li>
<li <?php if($_GET["qsort"]=='it_use_avg'&&$_GET["qorder"]=='desc'){?>class="active"<?php }?>>
<a href="#scl" onclick="set_sort('it_use_avg', 'desc'); return false;">평점높은순</a>
</li>
<li <?php if($_GET["qsort"]=='it_use_cnt'&&$_GET["qorder"]=='desc'){?>class="active"<?php }?>>
<a href="#scl" onclick="set_sort('it_use_cnt', 'desc'); return false;">후기많은순</a>
</li>
<li <?php if($_GET["qsort"]=='it_update_time'&&$_GET["qorder"]=='desc'){?>class="active"<?php }?>>
<a href="#scl" onclick="set_sort('it_update_time', 'desc'); return false;">최근등록순</a>
</li>
</ul>
<div class="tab-bottom-line"></div>
</section>
<div class="shop-search-category">
<ul class="list-inline">
<li><a href="#scl" onclick="set_ca_id(\"\"); return false;">전체분류 <span>(<?php echo $GLOBALS["total_cnt"]?>)</span></a></li>
<?php if($TPL_slist_1){foreach($TPL_VAR["slist"] as $TPL_V1){?>
<li><a href="#scl" onclick="set_ca_id('<?php echo $TPL_V1["ca_id"]?>'); return false;"><?php echo $TPL_V1["ca_name"]?> (<?php echo $TPL_V1["cnt"]?>)</a></li>
<?php }}?>
</ul>
</div>
<div>
<?php if(file_exists($GLOBALS["list_file"])){?>
<?php echo $TPL_VAR["list"]->set_view('it_img',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_id',false)?>
<?php echo $TPL_VAR["list"]->set_view('it_name',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_basic',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_cust_price',false)?>
<?php echo $TPL_VAR["list"]->set_view('it_price',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_icon',true)?>
<?php echo $TPL_VAR["list"]->set_view('sns',true)?>
<?php echo $TPL_VAR["list"]->run()?>
<?php }else{?>
<div class="text-center"><p><?php echo $GLOBALS["list_file"]?> 파일을 찾을 수 없습니다.<br>관리자에게 알려주시면 감사하겠습니다.</p></div>
<?php }?>
<?php echo eb_paging('basic')?>
</div>
</div>
<script>
function set_sort(qsort, qorder)
{
var f = document.frmdetailsearch;
f.qsort.value = qsort;
f.qorder.value = qorder;
f.submit();
}
function set_ca_id(qcaid)
{
var f = document.frmdetailsearch;
f.qcaid.value = qcaid;
f.submit();
}
</script>