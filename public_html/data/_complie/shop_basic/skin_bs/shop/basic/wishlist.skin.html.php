<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/wishlist.skin.html 000003822 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
.shop-wishlist .eyoom-form .radio i,.shop-cart .eyoom-form .checkbox i{top:2px}
.shop-wishlist .table-list-eb .sod-ws-img{width:50px}
.shop-wishlist .table-list-eb td img{display:block;width:100%;max-width:100%;height:auto}
.shop-wishlist .table-list-eb .table tbody > tr > td {text-align:center}
</style>
<div class="shop-wishlist">
<form name="fwishlist" method="post" action="./cartupdate.php" class="eyoom-form">
<input type="hidden" name="act" value="multi">
<input type="hidden" name="sw_direct" value="">
<input type="hidden" name="prog" value="wish">
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb margin-bottom-20">
<div class="table-responsive">
<table class="table table-bordered">
<thead>
<tr>
<th width="30">선택</th>
<th>이미지</th>
<th>상품명</th>
<th>보관일시</th>
<th>삭제</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<tr>
<td>
<?php if(is_soldout($TPL_V1["it_id"])){?>
품절
<?php }else{?>
<label for="chk_it_id_<?php echo $TPL_K1?>" class="sound_only"><?php echo $TPL_V1["it_name"]?></label>
<label class="checkbox">
<input type="checkbox" name="chk_it_id[<?php echo $TPL_K1?>]" value="1" id="chk_it_id_<?php echo $TPL_K1?>" onclick="out_cd_check(this, '<?php echo $TPL_V1["out_cd"]?>');"><i></i>
</label>
<?php }?>
<input type="hidden" name="it_id[<?php echo $TPL_K1?>]" value="<?php echo $TPL_V1["it_id"]?>">
<input type="hidden" name="io_type[<?php echo $TPL_V1["it_id"]?>][0]" value="0">
<input type="hidden" name="io_id[<?php echo $TPL_V1["it_id"]?>][0]" value="">
<input type="hidden" name="io_value[<?php echo $TPL_V1["it_id"]?>][0]" value="<?php echo $TPL_V1["it_name"]?>">
<input type="hidden"   name="ct_qty[<?php echo $TPL_V1["it_id"]?>][0]" value="1">
</td>
<td class="sod-ws-img"><a href="./item.php?it_id=<?php echo $TPL_V1["it_id"]?>"><?php echo $TPL_V1["image"]?></a></td>
<td><a href="./item.php?it_id=<?php echo $TPL_V1["it_id"]?>"><?php echo stripslashes($TPL_V1["it_name"])?></a></td>
<td><?php echo $TPL_V1["wi_time"]?></td>
<td><a href="./wishupdate.php?w=d&amp;wi_id=<?php echo $TPL_V1["wi_id"]?>" class="btn-e btn-e-xs btn-e-dark color-white">삭제</a></td>
</tr>
<?php }}else{?>
<tr><td colspan="5" class="text-center">보관함이 비었습니다.</td></tr>
<?php }?>
</tbody>
</table>
</div>
</div>
<div class="text-center">
<button type="submit" class="btn-e btn-e-red btn-e-lg" onclick="return fwishlist_check(document.fwishlist,'');">장바구니 담기</button>
<button type="submit" class="btn-e btn-e-dark btn-e-lg" onclick="return fwishlist_check(document.fwishlist,'direct_buy');">주문하기</button>
</div>
</form>
</div>
<script>
function out_cd_check(fld, out_cd)
{
if (out_cd == 'no'){
alert("옵션이 있는 상품입니다.\n\n상품을 클릭하여 상품페이지에서 옵션을 선택한 후 주문하십시오.");
fld.checked = false;
return;
}
if (out_cd == 'tel_inq'){
alert("이 상품은 전화로 문의해 주십시오.\n\n장바구니에 담아 구입하실 수 없습니다.");
fld.checked = false;
return;
}
}
function fwishlist_check(f, act)
{
var k = 0;
var length = f.elements.length;
for(i=0; i<length; i++) {
if (f.elements[i].checked) {
k++;
}
}
if(k == 0)
{
alert("상품을 하나 이상 체크 하십시오");
return false;
}
if (act == "direct_buy")
{
f.sw_direct.value = 1;
}
else
{
f.sw_direct.value = 0;
}
return true;
}
</script>