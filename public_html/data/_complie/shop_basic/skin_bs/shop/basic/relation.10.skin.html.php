<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/relation.10.skin.html 000003642 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; // ?>
<div class="product-e1">
<div class="shopNavigation margin-bottom-25">
<a class="owl-btn prev-s3 rounded-x"><i class="fa fa-angle-left"></i></a>
<a class="owl-btn next-s3 rounded-x"><i class="fa fa-angle-right"></i></a>
</div>
<div class="owl-slider-wrap">
<ul class="list-inline owl-slider-shop3 owl-carousel">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
<li class="item">
<div class="product-img">
<?php if($TPL_VAR["href"]){?>
<a href="<?php echo $TPL_V1["href"]?>">
<?php }?>
<?php echo $TPL_V1["it_image"]?>
<?php if($TPL_VAR["href"]){?>
</a>
<?php }?>
<?php if($TPL_VAR["view_it_icon"]){?>
<?php echo $TPL_V1["it_icon"]?>
<?php }?>
</div>
<div class="product-description product-description-brd margin-bottom-20">
<div class="overflow-h">
<div class="product-description-title">
<?php if($TPL_VAR["href"]){?>
<h4 class="title-price">
<a href="<?php echo $TPL_V1["href"]?>">
<?php }?>
<?php if($TPL_VAR["view_it_name"]){?><?php echo stripslashes($TPL_V1["it_name"])?><?php }?>
<?php if($TPL_VAR["href"]){?>
</a>
</h4>
<?php }?>
<?php if($TPL_VAR["view_it_cust_price"]||$TPL_VAR["view_it_price"]){?>
<div class="product-price">
<?php if($TPL_VAR["view_it_price"]){?>
<span class="title-price">₩ <?php echo $TPL_V1["it_tel_inq"]?></span>
<?php }?>
<?php if($TPL_VAR["view_it_cust_price"]&&$TPL_V1["it_cust_price"]){?>
<span class="title-price line-through">₩ <?php echo $TPL_V1["it_cust_price"]?></span>
<?php }?>
</div>
<?php }?>
<?php if($TPL_VAR["view_it_id"]){?>
<span class="type text-uppercase"><?php echo stripslashes($TPL_V1["it_id"])?></span>
<?php }?>
</div>
</div>
</div>
</li>
<?php }}else{?>
<p>관련상품이 없습니다.</p>
<?php }?>
</ul>
</div>
</div>
<style>
.product-e1 .shopNavigation {text-align:right;margin-top:-45px;margin-bottom:30px}
.product-e1 .shopNavigation a.owl-btn {color:#c5c5c5;width:22px;height:22px;font-size:14px;cursor:pointer;line-height:20px;text-align:center;display:inline-block;border:1px solid #c5c5c5;border-radius:0 !important}
.product-e1 .shopNavigation a.owl-btn:hover {color:#000;border-color:#000;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out;background:#fff}
.product-e1 .shopNavigation a.owl-btn.prev i {margin-left:-2px}
.product-e1 .shopNavigation a.owl-btn.next i {margin-right:-2px}
.product-e1 .list-inline {margin-left:0}
.product-e1 .owl-slider-wrap {padding:0}
.product-e1 .item {margin:0 10px;padding-bottom:5px}
.product-e1 .product-img {position:relative}
.product-e1 .product-img img {width:100%;display:block;max-width:100%;height:auto}
.product-description {overflow:hidden}
.product-description-brd {}
.product-description-title {padding:8px 0;text-align:center}
.product-description .title-price {color:#c9253c;font-size:13px;font-weight:bold}
.product-description h4.title-price {margin:2px 0 0}
.product-description h4.title-price a {color:#000;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden}
.product-description h4.title-price a:hover {color:#FF2900}
.product-e1 .product-description .product-price {padding:0;margin-bottom:10px}
.product-description .line-through {color:#a5a5a5;text-decoration:line-through;margin-left:7px}
.product-description .type {color:#757575;display:block;font-size:12px}
</style>