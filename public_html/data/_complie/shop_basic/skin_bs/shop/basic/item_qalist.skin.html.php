<?php /* Template_ 2.2.8 2019/10/27 16:21:46 /home/hellomilja/www/eyoom/theme/shop_basic/skin_bs/shop/basic/item_qalist.skin.html 000005828 */  $this->include_("eb_paging");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
.shop-product-qa-list .panel {border:0;border-top:1px solid #d5d5d5;box-shadow:none}
.shop-product-qa-list .panel:last-child {border-bottom:1px solid #d5d5d5}
.shop-product-qa-list .panel-group .panel+.panel {margin-top:0}
.shop-product-qa-list .panel-title {position:relative}
.shop-product-qa-list .panel-title a {color:#000;background:#fcfcfc}
.shop-product-qa-list .panel-title .product-qa-img {width:45px;margin-right:10px}
.shop-product-qa-list .panel-title .product-qa-img img {display:block;width:100%;max-width:100%;height:auto}
.shop-product-qa-list .panel-title .title-subj {font-size:12px;margin-bottom:5px;padding-right:30px}
.shop-product-qa-list .panel-title .divide {color:#c5c5c5;margin-left:7px;margin-right:7px}
.shop-product-qa-list .panel-title .indicator {font-size:11px;width:26px;height:16px;line-height:15px;background:#474A5E;border:1px solid #2E3340;text-align:center;color:#fff;position:absolute;top:11px;right:0}
.shop-product-qa-list .panel-title .indicator.fa-angle-up {background:#FF2900;border:1px solid #DE2600}
.shop-product-qa-list .panel-heading a {padding:10px 0 5px}
.shop-product-qa-list .panel-body {padding:15px 0;border-top:1px dotted #d5d5d5}
.shop-product-qa-list .sit_qaa_done {color:#FF2900}
</style>
<fieldset class="margin-bottom-20">
<form method="get" action="<?php echo $_SERVER['PHP_SELF']?>" class="eyoom-form">
<div class="row">
<section class="col col-12" style="text-align: center;margin: 30px 0px 30px 0px;">
<div class="font-size-11"><strong>Note:</strong> 타이틀 이미지를 클릭하시면 해당상품으로 이동됩니다.</div>
</section>
</div>
<div class="row">
<section class="col col-2">
</section>
<section class="col col-3">
<label for="sfl" class="sound_only">검색항목<strong class="sound_only"> 필수</strong></label>
<lavel class="select">
<select name="sfl" id="sfl" required class="form-control">
<option value="">선택</option>
<option value="b.it_name"    <?php echo get_selected($GLOBALS["sfl"],"b.it_name",true)?>>상품명</option>
<option value="a.it_id"      <?php echo get_selected($GLOBALS["sfl"],"a.it_id")?>>상품코드</option>
<option value="a.iq_subject" <?php echo get_selected($GLOBALS["sfl"],"a.is_subject")?>>문의제목</option>
<option value="a.iq_question"<?php echo get_selected($GLOBALS["sfl"],"a.iq_question")?>>문의내용</option>
<option value="a.iq_name"    <?php echo get_selected($GLOBALS["sfl"],"a.it_id")?>>작성자명</option>
<option value="a.mb_id"      <?php echo get_selected($GLOBALS["sfl"],"a.mb_id")?>>작성자아이디</option>
</select>
<i></i>
</lavel>
</section>
<section class="col col-3 input-group">
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<lavel class="input"><input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required class="form-control"></lavel>
<span class="input-group-btn">
<button type="submit" value="검색" class="btn btn-default btn-e-group">검색</button>
</span>
</section>
<section class="col col-3">
<a href="<?php echo $_SERVER['PHP_SELF']?>" class="btn-e btn-e-dark btn-e-group">전체보기</a>
</section>
<section class="col col-1">
</section>
</div>
</form>
</fieldset>
<section class="shop-product-qa-list">
<div class="panel-group acc-v1" id="porduct-qa">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">
<div class="pull-left product-qa-img">
<a href="<?php echo $TPL_V1["it_href"]?>">
<?php echo get_it_image($TPL_V1["it_id"], 200)?>
<span class="sound_only"><?php echo $TPL_V1["it_name"]?></span>
</a>
</div>
<a class="accordion-toggle" data-toggle="collapse" data-parent="#porduct-qa" href="#product_qa_<?php echo $TPL_K1?>">
<p class="title-subj"><strong><?php echo $TPL_V1["iq_subject"]?></strong></p>
<div class="pull-left">
<span class="font-size-12 color-grey">
<?php echo $TPL_V1["iq_name"]?><span class="divide">|</span><?php echo substr($TPL_V1["iq_time"], 0, 10)?>
</span>
</div>
<span class="font-size-12 pull-right">
상태: <strong class="<?php echo $TPL_V1["iq_style"]?>"><?php echo $TPL_V1["iq_stats"]?></strong>
</span>
<i class="indicator fa fa-angle-down pull-right"></i>
<div class="clearfix"></div>
</a>
</div>
</div>
<div id="product_qa_<?php echo $TPL_K1?>" class="panel-collapse collapse">
<div class="panel-body">
<p><strong class="color-black margin-right-5">문의</strong> <span class="color-black">-----</span> <?php echo $TPL_V1["iq_question"]?></p>
<div class="margin-hr-10"></div>
<p><strong class="color-red margin-right-5">답변</strong> <span class="color-red">-----</span> <span class="font-size-12 display-block margin-top-10"><?php if(!$TPL_V1["is_secret"]){?><?php echo $TPL_V1["iq_answer"]?><?php }else{?>비밀글로 보호된 답변입니다.<?php }?></span></p>
</div>
</div>
</div>
<?php }}else{?>
<p class="text-center">상품문의가 없습니다.</p>
<?php }?>
</div>
</section>
<?php echo eb_paging('basic')?>
<script src="/js/viewimageresize.js"></script>
<script>
function toggleAngle(e) {
$(e.target)
.prev('.panel-heading')
.find("i.indicator")
.toggleClass('fa-angle-up fa-angle-down');
}
$('#porduct-qa').on('hidden.bs.collapse', toggleAngle);
$('#porduct-qa').on('shown.bs.collapse', toggleAngle);
$(window).on("load", function() {
$(".panel-body").viewimageresize2();
});
</script>