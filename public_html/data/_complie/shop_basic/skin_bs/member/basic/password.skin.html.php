<?php /* Template_ 2.2.8 2017/10/22 23:02:38 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/member/basic/password.skin.html 000004015 */ ?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/custom.css" type="text/css" media="screen">',0);
?>
<div class="password-confirm">
<h5 class="margin-bottom-20"><strong>비밀번호 확인</strong></h5>
<div class="tab-e1">
<ul class="nav nav-tabs">
<li class="active"><a>비밀번호 입력</a></li>
</ul>
<div class="tab-content">
<h6><strong>해당글: <span class="color-red"><?php echo $TPL_VAR["g5"]["title"]?></span></strong></h6>
<!-- 비밀번호 확인 시작 -->
<form name="fboardpassword" action="<?php echo $GLOBALS["action"]?>" method="post" class="eyoom-form">
<input type="hidden" name="w" value="<?php echo $GLOBALS["w"]?>">
<input type="hidden" name="bo_table" value="<?php echo $GLOBALS["bo_table"]?>">
<input type="hidden" name="wr_id" value="<?php echo $GLOBALS["wr_id"]?>">
<input type="hidden" name="comment_id" value="<?php echo $GLOBALS["comment_id"]?>">
<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
<section>
<?php if($GLOBALS["w"]=='u'){?>
<div class="note margin-bottom-10"><strong>Note:</strong> 작성자만 글을 수정할 수 있습니다. 작성자 본인이라면, 글 작성시 입력한 비밀번호를 입력하여 글을 수정할 수 있습니다.</div>
<?php }elseif($GLOBALS["w"]=='d'||$GLOBALS["w"]=='x'){?>
<div class="note margin-bottom-10"><strong>Note:</strong> 작성자만 글을 삭제할 수 있습니다. 작성자 본인이라면, 글 작성시 입력한 비밀번호를 입력하여 글을 삭제할 수 있습니다.</div>
<?php }else{?>
<div class="note margin-bottom-10"><strong>Note:</strong> 비밀글 기능으로 보호된 글입니다. 작성자와 관리자만 열람하실 수 있습니다. 본인이라면 비밀번호를 입력하세요.</div>
<?php }?>
</section>
<div class="margin-hr-10"></div>
<section>
<label for="pw_wr_password" class="label">비밀번호<strong class="sound_only">필수</strong></label>
<label class="input">
<i class="icon-prepend fa fa-lock"></i>
<i class="icon-append fa fa-question-circle"></i>
<input type="password" name="wr_password" id="password_wr_password" required size="15" maxLength="20">
<b class="tooltip tooltip-top-right">비밀번호 입력</b>
</label>
</section>
<div class="margin-hr-10"></div>
<div class="text-center margin-bottom-20">
<input type="submit" value="확인" class="btn-e btn-e-yellow btn-e-lg">
</div>
</form>
<!-- 비밀번호 확인 끝 -->
<div class="margin-bottom-20"></div>
<div class="text-center">
<a href="<?php echo $GLOBALS["return_url"]?>" class="btn-e btn-e-dark">돌아가기</a>
</div>
</div>
</div>
</div>
<style>
.margin-hr-10 {height:1px;border-top:1px dotted #ddd;margin:10px 0}
.password-confirm {padding:15px;font-size:12px}
</style>
<script type="text/javascript" src="/eyoom/theme/shop_basic/plugins/eyoom-form/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/plugins/eyoom-form/plugins/jquery-form/jquery.form.min.js"></script>