<?php /* Template_ 2.2.8 2015/12/11 17:03:46 /home/hellomilja.com/www/eyoom/theme/shop_basic/main/index_bs.html 000005127 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가 ?>
<div class="main-banner-slider margin-bottom-20 hidden-xs">
<div class="owl-navi">
<a class="owl-btn prev-main-banner"><i class="fa fa-angle-left"></i></a>
<a class="owl-btn next-main-banner"><i class="fa fa-angle-right"></i></a>
</div>
<div class="owl-slider-main-banner">
<article class="item">
<a href="#">
<img src="/eyoom/theme/shop_basic/image/banner_slider/banner_slider_1.jpg">
<div class="banner-slider-text">
<h1><?php echo $TPL_VAR["config"]["cf_title"]?> OPEN!</h1>
<p>Lorem ipsum dulor sit amet, consectetur sequi ex quam sunt delectus adipisicing elft.<br>Veniam aut tempore illum, dulor nemo quae nulla.</p>
</div>
</a>
</article>
<article class="item">
<a href="#">
<img src="/eyoom/theme/shop_basic/image/banner_slider/banner_slider_2.jpg">
<div class="banner-slider-text">
<h1><?php echo $TPL_VAR["config"]["cf_title"]?> EVENT!</h1>
<p>Lorem ipsum dulor sit amet, consectetur sequi ex quam sunt delectus adipisicing elft.<br>Veniam aut tempore illum, dulor nemo quae nulla.</p>
</div>
</a>
</article>
<article class="item">
<a href="#">
<img src="/eyoom/theme/shop_basic/image/banner_slider/banner_slider_3.jpg">
<div class="banner-slider-text">
<h1><?php echo $TPL_VAR["config"]["cf_title"]?> NOTICE!</h1>
<p>Lorem ipsum dulor sit amet, consectetur sequi ex quam sunt delectus adipisicing elft.<br>Veniam aut tempore illum, dulor nemo quae nulla.</p>
</div>
</a>
</article>
</div>
</div>
<div class="main-tab tab-latest-text-padding margin-bottom-20">
<div class="tab-e2">
<ul class="nav nav-tabs">
<li class="active"><a href="#main-tlt-1" data-toggle="tab">게시판명</a></li>
<li><a href="#main-tlt-2" data-toggle="tab">게시판명</a></li>
<li><a href="#main-tlt-3" data-toggle="tab">게시판명</a></li>
<li class="last"><a href="#main-tlt-4" data-toggle="tab">게시판명</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane fade active in" id="main-tlt-1">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_text','bo_table=게시판id||count=10||cut_subject=50')?>
</div>
</div>
<div class="tab-pane fade in" id="main-tlt-2">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_text','bo_table=게시판id||count=10||cut_subject=50')?>
</div>
</div>
<div class="tab-pane fade in" id="main-tlt-3">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_text','bo_table=게시판id||count=10||cut_subject=50')?>
</div>
</div>
<div class="tab-pane fade in" id="main-tlt-4">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_text','bo_table=게시판id||count=10||cut_subject=50')?>
</div>
</div>
</div>
</div>
</div>
<div class="row margin-bottom-20">
<div class="col-sm-12">
<?php echo $TPL_VAR["latest"]->latest_item('item_slider','title=Shopping Item||count=16||cut_name=50||width=400||type=1')?>
</div>
</div>
<div class="main-tab tab-latest-image-padding margin-bottom-20">
<div class="tab-e2">
<ul class="nav nav-tabs">
<li class="active"><a href="#main-tli-1" data-toggle="tab">게시판명</a></li>
<li><a href="#main-tli-2" data-toggle="tab">게시판명</a></li>
<li><a href="#main-tli-3" data-toggle="tab">게시판명</a></li>
<li class="last"><a href="#main-tli-4" data-toggle="tab">게시판명</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane fade active in" id="main-tli-1">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_image','bo_table=게시판id||count=10||cut_subject=50||img_view=y||bo_direct=n||img_width=300')?>
</div>
</div>
<div class="tab-pane fade in" id="main-tli-2">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_image','bo_table=게시판id||count=10||cut_subject=50||img_view=y||bo_direct=n||img_width=300')?>
</div>
</div>
<div class="tab-pane fade in" id="main-tli-3">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_image','bo_table=게시판id||count=10||cut_subject=50||img_view=y||bo_direct=n||img_width=300')?>
</div>
</div>
<div class="tab-pane fade in" id="main-tli-4">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_eyoom('tab_latest_image','bo_table=게시판id||count=10||cut_subject=50||img_view=y||bo_direct=n||img_width=300')?>
</div>
</div>
</div>
</div>
</div>
<div class="row margin-bottom-20">
<div class="col-sm-6 md-margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_eyoom('basic','title=게시판명||bo_table=게시판id||count=10||cut_subject=50')?>
</div>
<div class="col-sm-6 md-margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_eyoom('basic','title=게시판명||bo_table=게시판id||count=10||cut_subject=50')?>
</div>
</div>
<div>
<?php echo $TPL_VAR["latest"]->latest_eyoom('webzine','title=게시판명||bo_table=게시판id||count=4||cut_subject=50||img_view=y||img_width=300||content=y||cut_content=100')?>
</div>